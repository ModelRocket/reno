/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package util

import (
	"encoding/hex"
	"math/rand"
	"strings"
	"time"
	"unicode"
)

// ProperTitle handles properly title casing a string
func ProperTitle(input string) string {
	words := strings.Fields(input)
	smallwords := " a an on the to "

	for index, word := range words {
		if strings.Contains(smallwords, " "+word+" ") {
			words[index] = word
		} else {
			words[index] = strings.Title(word)
		}
	}
	return strings.Join(words, " ")
}

// RemoveWhitespace removes all whitespace from a string
func RemoveWhitespace(input string, exclude ...string) string {
	return strings.Map(func(r rune) rune {
		if len(exclude) > 0 && strings.ContainsRune(exclude[0], r) {
			return r
		}
		if unicode.IsSpace(r) {
			return -1
		}
		if unicode.IsPunct(r) {
			return -1
		}
		return r
	}, input)
}

// SubKeys splits a dot notation key into sub-parsts, i.e. foo.bar.baz returns
// []string{ "foo", "foo.bar", "foo.bar.baz" }
func SubKeys(key string) []string {
	parts := strings.Split(key, ".")
	keys := make([]string, 0)
	for _, part := range parts {
		if len(keys) > 0 {
			keys = append(keys, keys[len(keys)-1]+"."+part)
		} else {
			keys = append(keys, part)
		}
	}

	return keys
}

// Unquote does what strconv.Unquote should with single quotes
func Unquote(s string) string {
	if len(s) >= 2 {
		if c := s[len(s)-1]; s[0] == c && (c == '"' || c == '\'') {
			return s[1 : len(s)-1]
		}
	}
	return s
}

var src = rand.NewSource(time.Now().UnixNano())

const letterBytes = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
const (
	letterIdxBits = 6                    // 6 bits to represent a letter index
	letterIdxMask = 1<<letterIdxBits - 1 // All 1-bits, as many as letterIdxBits
	letterIdxMax  = 63 / letterIdxBits   // # of letter indices fitting in 63 bits
)

// RandomString returns a random string
func RandomString(n int) string {
	b := make([]byte, n)
	// A src.Int63() generates 63 random bits, enough for letterIdxMax characters!
	for i, cache, remain := n-1, src.Int63(), letterIdxMax; i >= 0; {
		if remain == 0 {
			cache, remain = src.Int63(), letterIdxMax
		}
		if idx := int(cache & letterIdxMask); idx < len(letterBytes) {
			b[i] = letterBytes[idx]
			i--
		}
		cache >>= letterIdxBits
		remain--
	}

	return string(b)
}

// RandomHex returns a random hex string
func RandomHex(n int) (string, error) {
	bytes := make([]byte, n)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	return hex.EncodeToString(bytes), nil
}

// MustRandomHex generates a random hex string or panic
func MustRandomHex(n int) string {
	rval, err := RandomHex(n)
	if err != nil {
		panic(err)
	}
	return rval
}

// CLen gets the C-string length
func CLen(n []byte) int {
	for i := 0; i < len(n); i++ {
		if n[i] == 0 {
			return i
		}
	}
	return len(n)
}

// CTrim trims a c function
func CTrim(str string) string {
	bstr := []byte(str)
	nullIndex := CLen(bstr)
	bstr = bstr[:nullIndex]
	return string(bstr)
}

// Slug slugifies a string
func Slug(val string) string {
	return strings.Map(func(r rune) rune {
		if unicode.IsSpace(r) {
			return -1
		}
		return r
	}, val)
}

// StringRemove removes a string from a string slice
func StringRemove(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}
