/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// Copyright © 2016 Charles Phillips <charles@doublerebel.com>.
//
// Use of this source code is governed by an MIT-style
// license that can be found in the LICENSE file.

package util

import (
	"reflect"
	"strings"
)

// FlattenSeparator is what is used by flatten to separate segments
var FlattenSeparator = "."

func Expand(value map[string]interface{}) map[string]interface{} {
	return ExpandPrefixed(value, "")
}

func ExpandPrefixed(value map[string]interface{}, prefix string) map[string]interface{} {
	m := make(map[string]interface{})
	ExpandPrefixedToResult(value, prefix, m)
	return m
}

func ExpandPrefixedToResult(value map[string]interface{}, prefix string, result map[string]interface{}) {
	if prefix != "" {
		prefix += FlattenSeparator
	}
	for k, val := range value {
		if !strings.HasPrefix(k, prefix) {
			continue
		}

		key := k[len(prefix):]
		idx := strings.Index(key, FlattenSeparator)
		if idx != -1 {
			key = key[:idx]
		}
		if _, ok := result[key]; ok {
			continue
		}
		if idx == -1 {
			result[key] = val
			continue
		}

		// It contains a period, so it is a more complex structure
		result[key] = ExpandPrefixed(value, k[:len(prefix)+len(key)])
	}
}

func Flatten(value interface{}, tags ...string) map[string]interface{} {
	return FlattenPrefixed(value, "", tags...)
}

func FlattenPrefixed(value interface{}, prefix string, tags ...string) map[string]interface{} {
	if len(tags) == 0 {
		tags = []string{"json"}
	}

	m := make(map[string]interface{})
	FlattenPrefixedToResult(value, prefix, m, tags)
	return m
}

func FlattenPrefixedToResult(value interface{}, prefix string, m map[string]interface{}, tags []string) {
	base := ""
	if prefix != "" {
		base = prefix + FlattenSeparator
	}
	if len(tags) == 0 {
		tags = []string{"json"}
	}

	original := reflect.ValueOf(value)
	kind := original.Kind()
	if kind == reflect.Ptr || kind == reflect.Interface {
		original = reflect.Indirect(original)
		kind = original.Kind()
	}

	if !original.IsValid() {
		if prefix != "" {
			m[prefix] = nil
		}
		return
	}

	t := original.Type()

	switch kind {
	case reflect.Map:
		if t.Key().Kind() != reflect.String {
			break
		}
		for _, childKey := range original.MapKeys() {
			childValue := original.MapIndex(childKey)
			key := strings.ToLower(childKey.String())
			FlattenPrefixedToResult(childValue.Interface(), base+key, m, tags)
		}
	case reflect.Struct:
		for i := 0; i < original.NumField(); i += 1 {
			childValue := original.Field(i)

			childKey := t.Field(i).Name

			field, _ := t.FieldByName(childKey)

			omit := false
			override := ""

			for _, tag := range tags {
				tagName, tagOpts := parseTag(field.Tag.Get(tag))
				if tagName == "-" {
					omit = true
					break
				}
				if tagName != "" {
					childKey = tagName
				} else {
					continue
				}

				// if the value is a zero value and the field is marked as omitempty do
				// not include
				if _, ok := tagOpts.Get("omitempty"); ok {
					zero := reflect.Zero(childValue.Type()).Interface()
					current := childValue.Interface()

					if reflect.DeepEqual(current, zero) {
						omit = true
					}
				}
				if b, ok := tagOpts.Get("base"); ok {
					override = b + FlattenSeparator
				}
				break
			}

			if omit {
				continue
			}

			if childValue.Kind() == reflect.Ptr && !childValue.IsNil() {
				childValue = childValue.Elem()
			}

			next := base + childKey
			if override != "" {
				next = override + childKey
			}
			FlattenPrefixedToResult(childValue.Interface(), next, m, tags)
		}
	default:
		if prefix != "" {
			prefix = strings.ToLower(prefix)
			m[prefix] = value
		}
	}
}

// tagOptions contains a slice of tag options
type tagOptions map[string]string

// Has returns true if the given option is available in tagOptions
func (t tagOptions) Get(opt string) (string, bool) {
	for tagOpt, v := range t {
		if tagOpt == opt {
			return v, true
		}
	}

	return "", false
}

// parseTag splits a struct field's tag into its name and a list of options
// which comes after a name. A tag is in the form of: "name,option1,option2".
// The name can be neglectected.
func parseTag(tag string) (string, tagOptions) {
	// tag is one of followings:
	// ""
	// "name"
	// "name,opt"
	// "name,opt,opt2"
	// ",opt"

	res := strings.Split(tag, ",")

	opts := make(tagOptions)
	for _, opt := range res[1:] {
		parts := strings.Split(opt, "=")
		if len(parts) == 1 {
			opts[parts[0]] = "true"
		} else if len(parts) == 2 {
			opts[parts[0]] = parts[1]
		}
	}

	return res[0], opts
}
