/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package util

// MinInt64 returns the min of two int64 values
func MinInt64(x, y int64) int64 {
	if x < y {
		return x
	}
	return y
}

// MinInt64Ptr returns the pointer to the min of two int64 values
func MinInt64Ptr(x, y int64) *int64 {
	if x < y {
		return &x
	}
	return &y
}

// MaxInt64 returns the max of two int64 values
func MaxInt64(x, y int64) int64 {
	if x > y {
		return x
	}
	return y
}

// MaxInt64Ptr returns the pointer to the min of two int64 values
func MaxInt64Ptr(x, y int64) *int64 {
	if x > y {
		return &x
	}
	return &y
}

// Min returns the min of two int64 values
func Min(x, y int) int {
	if x < y {
		return x
	}
	return y
}

// Max returns the max of two int64 values
func Max(x, y int) int {
	if x > y {
		return x
	}
	return y
}

// MinFloat32 returns the min of two float32 values
func MinFloat32(x, y float32) float32 {
	if x < y {
		return x
	}
	return y
}

// MaxFloat32 returns the max of two float32 values
func MaxFloat32(x, y float32) float32 {
	if x > y {
		return x
	}
	return y
}

// PegFloat32 constrains a value between two boudaries
func PegFloat32(value, bottom, top float32) float32 {
	return MinFloat32(top, MaxFloat32(bottom, value))
}
