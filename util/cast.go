/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package util

import (
	"strings"

	"github.com/spf13/cast"
)

// ToSlice casts an interface to an []interace{}
func ToSlice(v interface{}) []interface{} {
	var r []interface{}

	switch t := v.(type) {
	case []string:
		for _, s := range t {
			r = append(r, s)
		}

	case []int:
		for _, s := range t {
			r = append(r, s)
		}

	case []int64:
		for _, s := range t {
			r = append(r, s)
		}
	case []float64:
		for _, s := range t {
			r = append(r, s)
		}
	default:
		return cast.ToSlice(v)
	}

	return r
}

// ToBool is an extended boolean caster
func ToBool(v interface{}) bool {
	if b, err := cast.ToBoolE(v); err == nil {
		return b
	}
	if sv, ok := v.(string); ok {
		if strings.ToLower(sv) == "yes" {
			return true
		}
	}
	return false
}
