/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package util

import (
	"crypto"
	"crypto/ecdsa"
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"errors"
	"fmt"

	"golang.org/x/crypto/pkcs12"
)

var (
	// ErrExpired is returned when an apns cert is no longer valid
	ErrExpired = errors.New("certificate has expired or is not yet valid")

	// ErrFailedToDecryptKey is returned when the ssl key cannot be decryped
	ErrFailedToDecryptKey = errors.New("failed to decrypt private key")

	// ErrFailedToParsePrivateKey is returned when the ssl private key cannot be parsed
	ErrFailedToParsePrivateKey = errors.New("failed to parse private key")
)

// ParsePKCS12Certificate parses a PKCS12 certificate and returns the public key and the certificate
func ParsePKCS12Certificate(certData []byte, password ...string) ([]byte, []byte, error) {
	passphrase := ""

	if len(password) > 0 {
		passphrase = password[0]
	}

	_, certificate, err := pkcs12.Decode(certData, passphrase)
	if err != nil {
		return nil, nil, err
	}

	var pemKey []byte
	var pemCert []byte

	blocks, _ := pkcs12.ToPEM(certData, passphrase)
	for _, block := range blocks {
		block.Headers = nil
		if block.Type == "PRIVATE KEY" {
			var pkey crypto.PrivateKey
			if x509.IsEncryptedPEMBlock(block) {
				bytes, err := x509.DecryptPEMBlock(block, []byte(passphrase))
				if err != nil {
					return nil, nil, ErrFailedToDecryptKey
				}
				pkey, err = parsePrivateKey(bytes)
				if err != nil {
					return nil, nil, err
				}
			} else {
				pkey, err = parsePrivateKey(block.Bytes)
				if err != nil {
					return nil, nil, err
				}
			}

			switch k := pkey.(type) {
			case *rsa.PrivateKey:
				pemKey = pem.EncodeToMemory(&pem.Block{Type: "RSA PRIVATE KEY", Bytes: x509.MarshalPKCS1PrivateKey(k)})
			case *ecdsa.PrivateKey:
				b, err := x509.MarshalECPrivateKey(k)
				if err != nil {
					return nil, nil, fmt.Errorf("Unable to marshal ECDSA private key: %v", err)
				}
				pemKey = pem.EncodeToMemory(&pem.Block{Type: "EC PRIVATE KEY", Bytes: b})
			default:
				pemKey = pem.EncodeToMemory(block)
			}
		} else if block.Type == "CERTIFICATE" {
			pemCert = pem.EncodeToMemory(block)
		}
	}

	if err = verifyCertificate(certificate); err != nil {
		return nil, nil, err
	}

	return pemKey, pemCert, nil
}

func verifyCertificate(cert *x509.Certificate) error {
	_, err := cert.Verify(x509.VerifyOptions{
		KeyUsages: []x509.ExtKeyUsage{
			x509.ExtKeyUsageAny,
		},
	})
	if err == nil {
		return nil
	}

	switch e := err.(type) {
	case x509.CertificateInvalidError:
		switch e.Reason {
		case x509.Expired:
			return ErrExpired
		default:
			return err
		}
	case x509.UnknownAuthorityError:
		// Apple cert isn't in the cert pool
		// ignoring this error
		return nil
	default:
		return err
	}
}

func parsePrivateKey(bytes []byte) (crypto.PrivateKey, error) {
	var key crypto.PrivateKey
	key, err := x509.ParsePKCS1PrivateKey(bytes)
	if err == nil {
		return key, nil
	}
	key, err = x509.ParsePKCS8PrivateKey(bytes)
	if err == nil {
		return key, nil
	}
	return nil, ErrFailedToParsePrivateKey
}
