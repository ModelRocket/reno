module gitlab.com/ModelRocket/reno

go 1.13

replace gitlab.com/ModelRocket/reno => ./

require (
	github.com/32bitkid/bitreader v1.0.1
	github.com/doublerebel/bellows v0.0.0-20160303004610-f177d92a03d3
	github.com/fatih/structs v1.1.0
	github.com/go-openapi/errors v0.19.3
	github.com/go-openapi/runtime v0.19.9
	github.com/go-openapi/strfmt v0.19.4
	github.com/go-openapi/swag v0.19.6
	github.com/go-openapi/validate v0.19.5
	github.com/gofrs/uuid v3.2.0+incompatible
	github.com/hashicorp/golang-lru v0.5.3
	github.com/imdario/mergo v0.3.8
	github.com/influxdata/influxdb v1.7.9
	github.com/jinzhu/gorm v1.9.11
	github.com/kr/pretty v0.1.0
	github.com/lib/pq v1.3.0
	github.com/mitchellh/mapstructure v1.1.2
	github.com/paulmach/orb v0.1.5
	github.com/robertkrimen/otto v0.0.0-20191219234010-c382bd3c16ff
	github.com/sirupsen/logrus v1.4.2
	github.com/spf13/cast v1.3.1
	github.com/twpayne/go-geom v1.0.5
	gitlab.com/ModelRocket/govaluate v3.0.1+incompatible
	golang.org/x/crypto v0.0.0-20191227163750-53104e6ec876
	gopkg.in/sourcemap.v1 v1.0.5 // indirect
	gopkg.in/yaml.v2 v2.2.7
)
