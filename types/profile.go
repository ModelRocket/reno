/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"

	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"

	openid "gitlab.com/ModelRocket/reno/types/openid"
	"gitlab.com/ModelRocket/reno/types/ptr"
)

// Profile A record profile, optional user/client data.
// swagger:model Profile
type Profile struct {
	openid.StandardClaims

	// extended attributes
	ExtendedAttributes map[string]string `json:"extendedAttributes,omitempty"`
}

// UnmarshalJSON unmarshals this object from a JSON structure
func (m *Profile) UnmarshalJSON(raw []byte) error {
	// AO0
	var aO0 openid.StandardClaims
	if err := swag.ReadJSON(raw, &aO0); err != nil {
		return err
	}
	m.StandardClaims = aO0

	// AO1
	var dataAO1 struct {
		ExtendedAttributes map[string]string `json:"extendedAttributes,omitempty"`
	}
	if err := swag.ReadJSON(raw, &dataAO1); err != nil {
		return err
	}

	m.ExtendedAttributes = dataAO1.ExtendedAttributes

	return nil
}

// MarshalJSON marshals this object to a JSON structure
func (m Profile) MarshalJSON() ([]byte, error) {
	_parts := make([][]byte, 0, 2)

	aO0, err := swag.WriteJSON(m.StandardClaims)
	if err != nil {
		return nil, err
	}
	_parts = append(_parts, aO0)

	var dataAO1 struct {
		ExtendedAttributes map[string]string `json:"extendedAttributes,omitempty"`
	}

	dataAO1.ExtendedAttributes = m.ExtendedAttributes

	jsonDataAO1, errAO1 := swag.WriteJSON(dataAO1)
	if errAO1 != nil {
		return nil, errAO1
	}
	_parts = append(_parts, jsonDataAO1)

	return swag.ConcatJSON(_parts...), nil
}

// Validate validates this profile
func (m *Profile) Validate(formats strfmt.Registry) error {
	var res []error

	// validation for a type composition with openid.StandardClaims
	if err := m.StandardClaims.Validate(formats); err != nil {
		res = append(res, err)
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *Profile) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Profile) UnmarshalBinary(b []byte) error {
	var res Profile
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

var (
	// DefaultProfile is a default profile object
	DefaultProfile = &Profile{
		StandardClaims: openid.StandardClaims{
			Locale: ptr.String("en-US"),
		},
	}
)

// Scan implements sql.Scanner
func (m *Profile) Scan(value interface{}) error {
	if m == nil {
		return nil
	}
	bytes, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("Failed to unmarshal JSONB value: %#v", value)
	}
	if err := json.Unmarshal(bytes, m); err != nil {
		return err
	}
	return nil
}

// Value implements the driver.Valuer interface
func (m *Profile) Value() (driver.Value, error) {
	if m == nil {
		return nil, nil
	}
	bytes, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return json.RawMessage(bytes).MarshalJSON()
}
