// Package ewkb implements Well Known Binary encoding and decoding.
// If you are encoding geometries in WKB to send to PostgreSQL/PostGIS, then
// you must specify binary_parameters=yes in the data source name that you pass
// to sql.Open.
package ewkb

import (
	"bytes"
	"database/sql/driver"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"io"

	"github.com/spf13/cast"

	"github.com/twpayne/go-geom"
	"github.com/twpayne/go-geom/encoding/wkbcommon"
	"gitlab.com/ModelRocket/reno/types"
	"gitlab.com/ModelRocket/reno/types/geo"
)

var (
	// XDR is big endian.
	XDR = wkbcommon.XDR
	// NDR is little endian.
	NDR = wkbcommon.NDR
)

const (
	ewkbZ    = 0x80000000
	ewkbM    = 0x40000000
	ewkbSRID = 0x20000000
)

// Encoder is the ewkb encoder struct
type Encoder struct {
	geo.Geometry
}

// HexEncoder is the ewkbhex encoder struct
type HexEncoder struct {
	Encoder
}

// NewEncoder returns a new encoder
func NewEncoder(g geo.Geometry, params ...types.Params) geo.Encoder {
	if len(params) > 0 {
		if cast.ToBool(params[0].Get("hex")) {
			return &HexEncoder{
				Encoder: Encoder{Geometry: g},
			}
		}
	}
	return &Encoder{
		Geometry: g,
	}
}

// Read reads an arbitrary geometry from r.
func Read(r io.Reader) (geo.Geometry, error) {
	var wkbByteOrder, err = wkbcommon.ReadByte(r)
	if err != nil {
		return nil, err
	}
	var byteOrder binary.ByteOrder
	switch wkbByteOrder {
	case wkbcommon.XDRID:
		byteOrder = XDR
	case wkbcommon.NDRID:
		byteOrder = NDR
	default:
		return nil, wkbcommon.ErrUnknownByteOrder(wkbByteOrder)
	}

	ewkbGeometryType, err := wkbcommon.ReadUInt32(r, byteOrder)
	if err != nil {
		return nil, err
	}
	t := wkbcommon.Type(ewkbGeometryType)

	layout := geom.NoLayout
	switch t & (ewkbZ | ewkbM) {
	case 0:
		layout = geom.XY
	case ewkbZ:
		layout = geom.XYZ
	case ewkbM:
		layout = geom.XYM
	case ewkbZ | ewkbM:
		layout = geom.XYZM
	default:
		return nil, wkbcommon.ErrUnknownType(t)
	}

	var srid uint32
	if ewkbGeometryType&ewkbSRID != 0 {
		srid, err = wkbcommon.ReadUInt32(r, byteOrder)
		if err != nil {
			return nil, err
		}
	}

	var rval geo.Geometry

	switch t &^ (ewkbZ | ewkbM | ewkbSRID) {
	case wkbcommon.PointID:
		flatCoords, err := wkbcommon.ReadFlatCoords0(r, byteOrder, layout.Stride())
		if err != nil {
			return nil, err
		}
		rval = &geo.Point{
			Coordinates: geo.Point2D(flatCoords),
		}
	case wkbcommon.LineStringID:
		ls := &geo.LineString{
			Coordinates: make([]geo.Point2D, 0),
		}
		flatCoords, err := wkbcommon.ReadFlatCoords1(r, byteOrder, layout.Stride())
		if err != nil {
			return nil, err
		}
		for i := 0; i < len(flatCoords); i += 2 {
			ls.Coordinates = append(ls.Coordinates, flatCoords[i:i+2])
		}
		rval = ls
	case wkbcommon.PolygonID:
		poly := &geo.Polygon{
			Coordinates: make([][]geo.Point2D, 0),
		}
		flatCoords, ends, err := wkbcommon.ReadFlatCoords2(r, byteOrder, layout.Stride())
		if err != nil {
			return nil, err
		}
		for _, v := range ends {
			parts := flatCoords[:v]
			sub := make([]geo.Point2D, 0)
			for i := 0; i < len(parts); i += 2 {
				sub = append(sub, parts[i:i+2])
			}
			poly.Coordinates = append(poly.Coordinates, sub)
		}
		rval = poly
	case wkbcommon.MultiPointID:
		n, err := wkbcommon.ReadUInt32(r, byteOrder)
		if err != nil {
			return nil, err
		}
		if limit := wkbcommon.MaxGeometryElements[1]; limit >= 0 && int(n) > limit {
			return nil, wkbcommon.ErrGeometryTooLarge{Level: 1, N: int(n), Limit: limit}
		}
		mp := &geo.MultiPoint{
			Coordinates: make([]geo.Point2D, 0),
		}
		for i := uint32(0); i < n; i++ {
			g, err := Read(r)
			if err != nil {
				return nil, err
			}
			p, ok := g.(*geo.Point)
			if !ok {
				return nil, wkbcommon.ErrUnexpectedType{Got: g, Want: &geom.Point{}}
			}
			mp.Coordinates = append(mp.Coordinates, p.Coordinates)
		}
		rval = mp
	case wkbcommon.MultiLineStringID:
		n, err := wkbcommon.ReadUInt32(r, byteOrder)
		if err != nil {
			return nil, err
		}
		if limit := wkbcommon.MaxGeometryElements[2]; limit >= 0 && int(n) > limit {
			return nil, wkbcommon.ErrGeometryTooLarge{Level: 2, N: int(n), Limit: limit}
		}
		mls := &geo.MultiLineString{
			Coordinates: make([][]geo.Point2D, 0),
		}
		for i := uint32(0); i < n; i++ {
			g, err := Read(r)
			if err != nil {
				return nil, err
			}
			ls, ok := g.(*geo.LineString)
			if !ok {
				return nil, wkbcommon.ErrUnexpectedType{Got: g, Want: &geom.LineString{}}
			}
			mls.Coordinates = append(mls.Coordinates, ls.Coordinates)
		}
		rval = mls
	case wkbcommon.MultiPolygonID:
		n, err := wkbcommon.ReadUInt32(r, byteOrder)
		if err != nil {
			return nil, err
		}
		if limit := wkbcommon.MaxGeometryElements[3]; limit >= 0 && int(n) > limit {
			return nil, wkbcommon.ErrGeometryTooLarge{Level: 3, N: int(n), Limit: limit}
		}
		mp := &geo.MultiPolygon{
			Coordinates: make([][][]geo.Point2D, 0),
		}
		for i := uint32(0); i < n; i++ {
			g, err := Read(r)
			if err != nil {
				return nil, err
			}
			p, ok := g.(*geo.Polygon)
			if !ok {
				return nil, wkbcommon.ErrUnexpectedType{Got: g, Want: &geom.Polygon{}}
			}
			mp.Coordinates = append(mp.Coordinates, p.Coordinates)
		}
		rval = mp
	default:
		return nil, wkbcommon.ErrUnsupportedType(ewkbGeometryType)
	}

	rval.SetLayout(layout)
	rval.SetByteOrder(byteOrder)
	rval.SetSRID(srid)
	return rval, nil
}

// Unmarshal unmrshals an arbitrary geometry from a []byte.
func Unmarshal(data []byte) (geo.Geometry, error) {
	return Read(bytes.NewBuffer(data))
}

// UnmarshalRaw raw unmarshals a raw geometry to a native geometry
func UnmarshalRaw(raw *geo.Raw) (geo.Geometry, error) {
	return Unmarshal(raw.Bytes())
}

// Write writes an arbitrary geometry to w.
func Write(w io.Writer, byteOrder binary.ByteOrder, g geo.Geometry) error {

	var ewkbByteOrder byte
	switch byteOrder {
	case XDR:
		ewkbByteOrder = wkbcommon.XDRID
	case NDR:
		ewkbByteOrder = wkbcommon.NDRID
	default:
		return wkbcommon.ErrUnsupportedByteOrder{}
	}
	if err := binary.Write(w, byteOrder, ewkbByteOrder); err != nil {
		return err
	}

	var ewkbGeometryType uint32
	switch g.(type) {
	case *geo.Point:
		ewkbGeometryType = wkbcommon.PointID
	case *geo.LineString:
		ewkbGeometryType = wkbcommon.LineStringID
	case *geo.Polygon:
		ewkbGeometryType = wkbcommon.PolygonID
	case *geo.MultiPoint:
		ewkbGeometryType = wkbcommon.MultiPointID
	case *geo.MultiLineString:
		ewkbGeometryType = wkbcommon.MultiLineStringID
	case *geo.MultiPolygon:
		ewkbGeometryType = wkbcommon.MultiPolygonID
	default:
		return geom.ErrUnsupportedType{Value: g}
	}

	switch g.Layout() {
	case geom.XY:
	case geom.XYZ:
		ewkbGeometryType |= ewkbZ
	case geom.XYM:
		ewkbGeometryType |= ewkbM
	case geom.XYZM:
		ewkbGeometryType |= ewkbZ | ewkbM
	default:
		return geom.ErrUnsupportedLayout(g.Layout())
	}
	srid := g.SRID()
	if srid != 0 {
		ewkbGeometryType |= ewkbSRID
	}
	if err := binary.Write(w, byteOrder, ewkbGeometryType); err != nil {
		return err
	}
	if ewkbGeometryType&ewkbSRID != 0 {
		if err := binary.Write(w, byteOrder, uint32(srid)); err != nil {
			return err
		}
	}

	switch g := g.(type) {
	case *geo.Point:
		return wkbcommon.WriteFlatCoords0(w, byteOrder, g.Coordinates)
	case *geo.LineString:
		flat := make(geo.Point2D, 0)
		for _, v := range g.Coordinates {
			flat = append(flat, v...)
		}
		return wkbcommon.WriteFlatCoords1(w, byteOrder, flat, g.Layout().Stride())
	case *geo.Polygon:
		ends := make([]int, 0)
		flat := make(geo.Point2D, 0)
		for _, v := range g.Coordinates {
			ends = append(ends, len(v)*2)
			for _, w := range v {
				flat = append(flat, w...)
			}
		}
		return wkbcommon.WriteFlatCoords2(w, byteOrder, flat, ends, g.Layout().Stride())
	case *geo.MultiPoint:
		n := len(g.Coordinates)
		if err := wkbcommon.WriteUInt32(w, byteOrder, uint32(n)); err != nil {
			return err
		}
		for i := 0; i < n; i++ {
			if err := Write(w, byteOrder, &geo.Point{Coordinates: g.Coordinates[i]}); err != nil {
				return err
			}
		}
		return nil
	case *geo.MultiLineString:
		n := len(g.Coordinates)
		if err := wkbcommon.WriteUInt32(w, byteOrder, uint32(n)); err != nil {
			return err
		}
		for i := 0; i < n; i++ {
			if err := Write(w, byteOrder, &geo.LineString{Coordinates: g.Coordinates[i]}); err != nil {
				return err
			}
		}
		return nil
	case *geo.MultiPolygon:
		n := len(g.Coordinates)
		if err := wkbcommon.WriteUInt32(w, byteOrder, uint32(n)); err != nil {
			return err
		}
		for i := 0; i < n; i++ {
			if err := Write(w, byteOrder, &geo.Polygon{Coordinates: g.Coordinates[i]}); err != nil {
				return err
			}
		}
		return nil
	default:
		return geom.ErrUnsupportedType{Value: g}
	}
}

// Marshal marshals an arbitrary geometry to a []byte.
func Marshal(g geo.Geometry, byteOrder binary.ByteOrder) ([]byte, error) {
	if raw, ok := g.(*geo.Raw); ok {
		return raw.Bytes(), nil
	}

	w := bytes.NewBuffer(nil)
	if err := Write(w, byteOrder, g); err != nil {
		return nil, err
	}
	return w.Bytes(), nil
}

// Scan implements the sql.Scanner interface
func (m *Encoder) Scan(data interface{}) error {
	b, ok := data.([]byte)
	if !ok {
		return errors.New("invalid type for Encoder")
	}
	g, err := Unmarshal(b)
	if err != nil {
		return err
	}
	m.Geometry = g
	return nil
}

// Scan implements the sql.Scanner interface
func (m *HexEncoder) Scan(data interface{}) error {
	b, ok := data.([]byte)
	if !ok {
		return errors.New("invalid type for HexEncoder")
	}
	d, err := hex.DecodeString(string(b))
	if err != nil {
		return err
	}
	return m.Encoder.Scan(d)
}

// Value implements the driver.Valuer interface
func (m Encoder) Value() (driver.Value, error) {
	data, err := Marshal(m.Geometry, wkbcommon.NDR)
	if err != nil {
		return nil, err
	}
	return driver.Value(data), nil
}

// Value implements the driver.Valuer interface
func (m HexEncoder) Value() (driver.Value, error) {
	data, err := Marshal(m.Geometry, wkbcommon.NDR)
	if err != nil {
		return nil, err
	}

	return driver.Value(hex.EncodeToString(data)), nil
}
