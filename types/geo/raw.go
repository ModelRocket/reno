/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package geo

import (
	"database/sql/driver"
	"encoding/hex"
)

// Raw is a raw geometry struct used to encode raw data
type Raw struct {
	geometry
	data []byte
}

// NewRaw returns a new raw geometry
func NewRaw(data []byte) *Raw {
	return &Raw{data: data}
}

// Type gets the type of this subtype
func (m *Raw) Type() GeometryType {
	return "Raw"
}

// SetType sets the type of this subtype
func (m *Raw) SetType(val GeometryType) {

}

// Scan implements the sql.Scanner interface
func (m *Raw) Scan(data []byte) error {
	m.data = data
	return nil
}

// Value implements the driver.Valuer interface.
func (m Raw) Value() (driver.Value, error) {
	return hex.EncodeToString(m.data), nil
}

// Bytes returns the raw geometry bytes
func (m *Raw) Bytes() []byte {
	return m.data
}
