// Code generated by go-swagger; DO NOT EDIT.

package geo

// This file was generated by the swagger tool.
// Editing this file might prove futile when you re-run the swagger generate command

import (
	strfmt "github.com/go-openapi/strfmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/validate"
)

// Point2D point2 d
// swagger:model Point2D
type Point2D []float64

// Validate validates this point2 d
func (m Point2D) Validate(formats strfmt.Registry) error {
	var res []error

	iPoint2DSize := int64(len(m))

	if err := validate.MinItems("", "body", iPoint2DSize, 2); err != nil {
		return err
	}

	if err := validate.MaxItems("", "body", iPoint2DSize, 2); err != nil {
		return err
	}

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}
