/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package binary

import (
	"fmt"

	"github.com/spf13/cast"
	"gitlab.com/ModelRocket/govaluate"
)

func (inst *Instance) evalValue(d *decodeContext) (interface{}, error) {
	if inst.Value == nil {
		return 0, nil
	}

	expr, err := govaluate.NewEvaluableExpressionWithResolver(*inst.Value, d)
	if err != nil {
		return 0, fmt.Errorf("invalid value expression for instance %q: %s", inst.ID, err.Error())
	}

	result, err := expr.Evaluate(d.ctx)
	if err != nil {
		return 0, fmt.Errorf("invalid value expression for instance %q: %s", inst.ID, err.Error())
	}
	return result, nil
}

func (inst *Instance) evalPos(d *decodeContext) (int, error) {
	if inst.Pos == nil {
		return 0, nil
	}

	expr, err := govaluate.NewEvaluableExpressionWithResolver(*inst.Pos, d)
	if err != nil {
		return 0, fmt.Errorf("invalid position expression for instance %q: %s", inst.ID, err.Error())
	}

	result, err := expr.Evaluate(d.ctx)
	if err != nil {
		return 0, fmt.Errorf("invalid position expression for instance %q: %s", inst.ID, err.Error())
	}
	pos, err := cast.ToIntE(result)
	if err != nil {
		return 0, fmt.Errorf("invalid position expression for instance %q: %s", inst.ID, err.Error())
	}
	return pos, nil
}
