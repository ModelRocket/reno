/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package binary

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"reflect"
	"regexp"
	"strings"
	"sync"

	"github.com/32bitkid/bitreader"
	lru "github.com/hashicorp/golang-lru"

	"github.com/spf13/cast"
	"gitlab.com/ModelRocket/govaluate"
	"gitlab.com/ModelRocket/reno/types"
	"gitlab.com/ModelRocket/reno/util"
	yaml "gopkg.in/yaml.v2"

	"github.com/robertkrimen/otto"
	_ "github.com/robertkrimen/otto/underscore"
)

type (
	// Sequence is a field definition
	Sequence struct {
		// ID is the label for the field
		ID string `yaml:"id"`

		// Type defines the type, width, and byte order or a custom define type
		Type string `yaml:"type"`

		// Size is the length of the field in bytes, or points to another sequence value
		Size *string `yaml:"size,omitempty"`

		// Repeat is used for array values
		Repeat *string `yaml:"repeat,omitempty"`

		// RepeatExpression is evaluated to determine the number of times to repeat
		RepeatExpression *string `yaml:"repeat-expr,omitempty"`

		// Omit when set will omit the sequence value from the final result
		Omit bool `yaml:"omit"`

		// Embed emebed the sequence in to the main object
		Embed bool `yaml:"embed"`

		baseType string
		width    uint
		endian   binary.ByteOrder
	}

	// Instance is an instance definition
	Instance struct {
		Sequence `yaml:",inline"`

		// Pos is the position of the data in the byte stream
		Pos *string `yaml:"pos,omitempty"`

		// Value is the computed value of the instance object
		Value *string `yaml:"value,omitempty"`

		posExpr *govaluate.EvaluableExpression
		valExpr *govaluate.EvaluableExpression
	}

	// Type is a type definition
	Type struct {
		Sequence  []*Sequence `yaml:"seq"`
		Instances []*Instance `yaml:"instances"`
	}

	// Meta provides meta-information for the spec
	Meta struct {
		ID       string `yaml:"id"`
		Endian   string `yaml:"endian"`
		Encoding string `yaml:"encoding"`
	}

	// Spec defines the specification
	Spec struct {
		Meta      Meta             `yaml:"meta"`
		Sequence  []*Sequence      `yaml:"seq"`
		Instances []*Instance      `yaml:"instances"`
		Types     map[string]*Type `yaml:"types,omitempty"`

		evalFuncs map[string]EvalFunc
		vm        *otto.Otto
	}

	decodeContext struct {
		*Spec
		evalFuncs map[string]govaluate.ExpressionFunction
		ctx       types.StringMap
		reader    bitreader.BitReader
		data      []byte
	}

	// EvalFunc is an evalutation function that can be used in specs
	EvalFunc func(ctx types.StringMap, args ...interface{}) (interface{}, error)
)

var (
	// builtin is used for custom methods for the decoding
	builtin = map[string]govaluate.ExpressionFunction{
		"seq": func(args ...interface{}) (interface{}, error) {
			if len(args) < 2 {
				return nil, errors.New("too few parameters")
			}

			if mm, ok := args[0].(types.StringMap); ok {
				key := cast.ToString(args[1])
				if v, ok := mm.GetValue(key); ok {
					return v.Interface(), nil
				}
				return nil, fmt.Errorf("key %q does not exist", key)
			}
			return nil, fmt.Errorf("not a map")
		},
	}

	typeReg = regexp.MustCompile(`(str[z]?)|([busf]{1})(\d+)(le|be)?`)

	evalFnCache *lru.Cache

	cacheLock sync.Mutex
)

func init() {
	evalFnCache, _ = lru.New(64)
}

// Decode decodes the data into the struct
func Decode(data []byte, out interface{}, evalFuncs ...interface{}) error {
	spec := &Spec{
		Sequence: make([]*Sequence, 0),
	}

	t := reflect.TypeOf(out)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	for i := 0; i < t.NumField(); i++ {
		field := t.Field(i)

		seq := &Sequence{
			ID: field.Name,
		}

		settings := parseTagSetting(field.Tag)
		endian := "le"
		if e, ok := settings["ENDIAN"]; ok {
			endian = e
		}

		if t, ok := settings["TYPE"]; ok {
			seq.Type = t
		} else {
			t := field.Type
			if t.Kind() == reflect.Ptr {
				t = t.Elem()
			}
			switch t.Kind() {
			case reflect.Uint8:
				seq.Type = "u1"
			case reflect.Uint16:
				seq.Type = "u2"
			case reflect.Uint32:
				seq.Type = "u4"
			case reflect.Uint64:
				seq.Type = "u8"
			case reflect.Uint:
				seq.Type = "u8"
			case reflect.Int8:
				seq.Type = "s1"
			case reflect.Int16:
				seq.Type = "s2"
			case reflect.Int32:
				seq.Type = "s4"
			case reflect.Int64:
				seq.Type = "s8"
			case reflect.Int:
				seq.Type = "s8"
			}

			seq.Type = seq.Type + endian
		}

		spec.Sequence = append(spec.Sequence, seq)
	}

	if err := spec.Initialize(evalFuncs...); err != nil {
		return err
	}

	return spec.decodeStruct(data, out)
}

// NewSpec parses and initializes a new binary spec
func NewSpec(data []byte, evalFuncs ...interface{}) (*Spec, error) {
	s := &Spec{
		evalFuncs: make(map[string]EvalFunc),
		vm:        otto.New(),
	}

	if err := yaml.Unmarshal(data, &s); err != nil {
		return nil, err
	}

	return s, s.Initialize(evalFuncs...)
}

// AddEvalFunc add an evaluator function
func (s *Spec) AddEvalFunc(name string, handler EvalFunc) {
	s.evalFuncs[name] = handler
}

// AddEvalJS adds javascript functions to the evaluator
func (s *Spec) AddEvalJS(data interface{}, exports ...string) error {
	exportName := "exports"
	if len(exports) > 0 {
		exportName = exports[0]
	}
	if _, err := s.vm.Run(data); err != nil {
		return err
	}

	exp, err := s.vm.Get(exportName)
	if err != nil {
		return err
	}

	if exp.IsObject() {
		obj := exp.Object()
		for _, key := range obj.Keys() {
			fn, err := obj.Get(key)
			if err != nil {
				continue
			}
			if !fn.IsFunction() {
				continue
			}

			cacheLock.Lock()
			if handler, ok := evalFnCache.Get(s.Meta.ID + key); ok {
				cacheLock.Unlock()
				s.AddEvalFunc(key, handler.(func(types.StringMap, ...interface{}) (interface{}, error)))
				continue
			}

			func(key string, fn otto.Value) {
				defer cacheLock.Unlock()
				handler := func(ctx types.StringMap, args ...interface{}) (interface{}, error) {
					val, err := fn.Call(otto.NullValue(), args...)
					if err != nil {
						return nil, err
					}
					if val.IsNull() {
						return nil, nil
					}
					v, err := val.Export()
					if err != nil {
						return nil, err
					}
					return v, nil
				}
				evalFnCache.Add(s.Meta.ID+key, handler)
				s.AddEvalFunc(key, handler)
			}(key, fn)
		}
	}
	return nil
}

// Initialize initializes the spec and adds the specified methods
func (s *Spec) Initialize(evalFuncs ...interface{}) error {
	if s.vm == nil {
		s.vm = otto.New()
	}
	if s.evalFuncs == nil {
		s.evalFuncs = make(map[string]EvalFunc)
	}
	if s.Types == nil {
		s.Types = make(map[string]*Type)
	}
	for _, f := range evalFuncs {
		switch m := f.(type) {
		case map[string]EvalFunc:
			for k, v := range m {
				s.AddEvalFunc(k, v)
			}
		case string:
			if err := s.AddEvalJS(m); err != nil {
				return err
			}
		case []byte:
			if err := s.AddEvalJS(m); err != nil {
				return err
			}
		case map[string]*Type:
			for k, v := range m {
				s.Types[k] = v
			}
		}
	}

	if s.Meta.Endian != "be" && s.Meta.Endian != "le" {
		s.Meta.Endian = "le"
	}

	for _, seq := range s.Sequence {
		if err := s.initializeSequence(seq); err != nil {
			return err
		}
	}

	for _, typ := range s.Types {
		if err := s.initializeType(typ); err != nil {
			return err
		}
	}

	for _, inst := range s.Instances {
		if err := s.initializeSequence(&inst.Sequence); err != nil {
			return err
		}
	}

	return nil
}

func (s *Spec) initializeSequence(seq *Sequence) error {
	parts := typeReg.FindAllStringSubmatch(seq.Type, -1)

	if len(parts) < 1 {
		parts = [][]string{{seq.Type}}
	}

	typeSlice := types.Slice(&parts[0])

	// set the defaults
	if s.Meta.Endian == "be" {
		seq.endian = binary.BigEndian
	} else {
		seq.endian = binary.LittleEndian
	}

	// compess the slice and promote to type to sequence
	typeSlice.Filter(func(index int, val interface{}) bool {
		return val.(string) != ""
	}).ForEach(func(index int, val interface{}) {
		if index == 1 {
			seq.baseType = cast.ToString(val)
		}
		if index == 2 {
			if seq.baseType == "b" {
				seq.width = cast.ToUint(val)
			} else {
				seq.width = cast.ToUint(val) * 8
			}
		}
		if index == 3 {
			switch cast.ToString(val) {
			case "be":
				seq.endian = binary.BigEndian
			default:
				seq.endian = binary.LittleEndian
			}
		}
	})

	return nil
}

func (s *Spec) initializeType(t *Type) error {
	for _, seq := range t.Sequence {
		if err := s.initializeSequence(seq); err != nil {
			return err
		}
	}

	for _, inst := range t.Instances {
		if err := s.initializeSequence(&inst.Sequence); err != nil {
			return err
		}
	}
	return nil
}

// Decode decodes the binary data to a map using the spec
func (s *Spec) Decode(data []byte) (types.StringMap, error) {
	ctx := &decodeContext{
		Spec:      s,
		ctx:       make(types.StringMap),
		evalFuncs: make(map[string]govaluate.ExpressionFunction),
		reader:    bitreader.NewReader(bytes.NewReader(data)),
		data:      data,
	}

	for k, v := range builtin {
		ctx.evalFuncs[k] = v
	}

	for k, v := range s.evalFuncs {
		ctx.evalFuncs[k] = func(f EvalFunc) govaluate.ExpressionFunction {
			return func(args ...interface{}) (interface{}, error) {
				return f(ctx.ctx, args...)
			}
		}(v)
	}

	ctx.evalFuncs["context"] = func(args ...interface{}) (interface{}, error) {
		if len(args) < 1 {
			return nil, errors.New("too few parameters")
		}

		key := cast.ToString(args[0])
		return ctx.ctx.Get(key), nil
	}

	rval := make(types.StringMap)

	if err := ctx.decode(rval); err != nil {
		return nil, err
	}

	return rval, nil
}

// DecodeStruct decodes the data to the struct
func (s *Spec) DecodeStruct(data []byte, out interface{}) error {
	m, err := s.Decode(data)
	if err != nil {
		return err
	}
	return util.MapStruct(out, m)
}

// DecodeStruct decode attempts to the specified structure
func (s *Spec) decodeStruct(data []byte, out interface{}) error {
	ctx := &decodeContext{
		Spec:      s,
		ctx:       make(types.StringMap),
		evalFuncs: make(map[string]govaluate.ExpressionFunction),
	}

	for k, v := range builtin {
		ctx.evalFuncs[k] = v
	}

	for k, v := range s.evalFuncs {
		ctx.evalFuncs[k] = func(f EvalFunc) govaluate.ExpressionFunction {
			return func(args ...interface{}) (interface{}, error) {
				return f(ctx.ctx, args...)
			}
		}(v)
	}

	ctx.evalFuncs["context"] = func(args ...interface{}) (interface{}, error) {
		if len(args) < 1 {
			return nil, errors.New("too few parameters")
		}

		key := cast.ToString(args[0])
		return ctx.ctx.Get(key), nil
	}

	return ctx.decode(out)
}

func (d *decodeContext) decode(out interface{}) error {
	set := func(id string, value interface{}) {
		if rval, ok := out.(types.StringMap); ok {
			rval[id] = value
		} else {
			rval := reflect.ValueOf(out)
			if rval.Kind() == reflect.Ptr {
				rval = rval.Elem()
			}
			rval.FieldByName(id).Set(reflect.ValueOf(value))
		}
	}

	for _, seq := range d.Sequence {
		val, err := d.readSequence(seq)
		if err != nil {
			return err
		}

		if m, ok := val.(types.StringMap); ok && seq.Embed {
			for k, v := range m {
				d.ctx.Set(k, v)
				set(k, v)
			}
		} else {
			d.ctx.Set(seq.ID, val)
			if !seq.Omit {
				set(seq.ID, val)
			}
		}
	}

	for _, inst := range d.Instances {
		val, err := d.readInstance(inst)
		if err != nil {
			return err
		}
		d.ctx.Set(inst.ID, val)

		if !inst.Omit {
			set(inst.ID, val)
		}
	}

	return nil
}

func (d *decodeContext) readSequence(seq *Sequence, parent ...string) (interface{}, error) {
	if sub, ok := d.Types[seq.Type]; ok {
		return d.readType(seq, sub, false, parent...)
	}

	if seq.Repeat != nil {
		return d.readSequenceArray(seq, parent...)
	}

	size, err := seq.evalSize(d)
	if err != nil {
		return nil, err
	}

	switch seq.baseType {
	case "str":
		rval := make([]byte, size)
		_, err := d.reader.Read(rval)

		return string(rval), err

	case "strz":
		strval, err := d.readStrz()
		return util.CTrim(string(strval)), err

	case "b":
		return d.reader.Read64(seq.width)

	case "u":
		switch seq.width {
		case 8:
			return d.readU8()
		case 16:
			return d.readU16(seq.endian)
		case 32:
			return d.readU32(seq.endian)
		case 64:
			return d.readU64(seq.endian)
		}

	case "s":
		switch seq.width {
		case 8:
			return d.readS8()
		case 16:
			return d.readS16(seq.endian)
		case 32:
			return d.readS32(seq.endian)
		case 64:
			return d.readS64(seq.endian)
		}

	case "f":
		switch seq.width {
		case 32:
			return d.readF32(seq.endian)
		case 64:
			return d.readF64(seq.endian)
		default:
			return nil, fmt.Errorf("invalid sequence type: %q, bad float width %d", seq.baseType, seq.width)
		}
	}

	rval := make([]byte, size)
	_, err = d.reader.Read(rval)

	return rval, err
}

func (d *decodeContext) readU8() (uint8, error) {
	return d.reader.Read8(8)
}

func (d *decodeContext) readU16(b binary.ByteOrder) (uint16, error) {
	switch b {
	case binary.BigEndian:
		return d.reader.Read16(16)
	case binary.LittleEndian:
		b1, err := d.reader.Read16(8)
		if err != nil {
			return 0, err
		}
		b2, err := d.reader.Read16(8)
		if err != nil {
			return 0, err
		}
		return ((b2 << 8) | b1), nil
	}

	return 0, nil
}

func (d *decodeContext) readU32(b binary.ByteOrder) (uint32, error) {
	switch b {
	case binary.BigEndian:
		return d.reader.Read32(32)
	case binary.LittleEndian:
		b1, err := d.reader.Read32(8)
		if err != nil {
			return 0, err
		}
		b2, err := d.reader.Read32(8)
		if err != nil {
			return 0, err
		}
		b3, err := d.reader.Read32(8)
		if err != nil {
			return 0, err
		}
		b4, err := d.reader.Read32(8)
		if err != nil {
			return 0, err
		}
		return ((b4 << 24) | (b3 << 16) | (b2 << 8) | b1), nil
	}

	return 0, nil
}

func (d *decodeContext) readU64(b binary.ByteOrder) (uint64, error) {
	switch b {
	case binary.BigEndian:
		return d.reader.Read64(64)
	case binary.LittleEndian:
		b1, err := d.reader.Read64(8)
		if err != nil {
			return 0, err
		}
		b2, err := d.reader.Read64(8)
		if err != nil {
			return 0, err
		}
		b3, err := d.reader.Read64(8)
		if err != nil {
			return 0, err
		}
		b4, err := d.reader.Read64(8)
		if err != nil {
			return 0, err
		}
		b5, err := d.reader.Read64(8)
		if err != nil {
			return 0, err
		}
		b6, err := d.reader.Read64(8)
		if err != nil {
			return 0, err
		}
		b7, err := d.reader.Read64(8)
		if err != nil {
			return 0, err
		}
		b8, err := d.reader.Read64(8)
		if err != nil {
			return 0, err
		}
		return ((b8 << 56) | (b7 << 48) | (b6 << 40) | (b5 << 32) | (b4 << 24) | (b3 << 16) | (b2 << 8) | b1), nil
	}

	return 0, nil
}

func (d *decodeContext) readS8() (int8, error) {
	val, err := d.reader.Read8(8)
	if err != nil {
		return 0, err
	}
	return cast.ToInt8(val), nil
}

func (d *decodeContext) readS16(b binary.ByteOrder) (int16, error) {
	v, err := d.readU16(b)
	return cast.ToInt16(v), err
}

func (d *decodeContext) readS32(b binary.ByteOrder) (int32, error) {
	v, err := d.readU32(b)
	return cast.ToInt32(v), err
}

func (d *decodeContext) readS64(b binary.ByteOrder) (int64, error) {
	v, err := d.readU64(b)
	return cast.ToInt64(v), err
}

func (d *decodeContext) readF32(b binary.ByteOrder) (float32, error) {
	v, err := d.readU32(b)
	return cast.ToFloat32(v), err
}

func (d *decodeContext) readF64(b binary.ByteOrder) (float64, error) {
	v, err := d.readU64(b)
	return cast.ToFloat64(v), err
}

func (d *decodeContext) readStrz() (string, error) {
	out := make([]byte, 0)

	for {
		c, err := d.reader.Read8(8)
		if err != nil {
			return string(out), err
		}
		out = append(out, c)
		if c == 0 {
			break
		}
	}

	return string(out), nil
}

func (d *decodeContext) readType(seq *Sequence, typ *Type, inArray bool, parent ...string) (interface{}, error) {
	if seq.Repeat != nil && !inArray {
		return d.readTypeArray(seq, typ, parent...)
	}

	rval := make(types.StringMap)

	base := seq.ID
	if len(parent) > 0 {
		parent = append(parent, seq.ID)
		base = strings.Join(parent, ".")
	}

	for _, s := range typ.Sequence {
		val, err := d.readSequence(s, base)
		if err != nil {
			return nil, err
		}

		if !inArray {
			if m, ok := val.(types.StringMap); ok && seq.Embed {
				for k, v := range m {
					d.ctx.Set(k, v)
					if !seq.Omit {
						rval.Set(k, v)
					}
				}
			} else {
				if !seq.Embed {
					d.ctx.Set(base+"."+s.ID, val)
				} else {
					d.ctx.Set(s.ID, val)
				}
				if !seq.Omit {
					rval.Set(s.ID, val)
				}
			}
		}
	}

	// aggregate the context
	if inArray {
		if !d.ctx.IsSet(base) {
			d.ctx.Set(base, []interface{}{&rval})
		} else {
			tmp := d.ctx.Get(base)
			if arr, ok := tmp.([]interface{}); ok {
				arr = append(arr, &rval)
				d.ctx.Set(base, arr)
			}
		}
		d.ctx.Set("_index", types.Slice(d.ctx.Get(base)).Len()-1)
		d.ctx.Set("_inst", rval)
	}

	for _, inst := range typ.Instances {
		val, err := d.readInstance(inst)
		if err != nil {
			return nil, err
		}

		if !inArray {
			d.ctx.Set(base+"."+inst.ID, val)
		}
		if !inst.Omit {
			rval.Set(inst.ID, val)
		}
	}

	return rval, nil
}

func (d *decodeContext) readTypeArray(seq *Sequence, typ *Type, base ...string) (interface{}, error) {
	rval := make([]interface{}, 0)
	rem := -1

	if seq.RepeatExpression != nil && *seq.Repeat == "expr" {
		expr, err := govaluate.NewEvaluableExpressionWithResolver(*seq.RepeatExpression, d)
		if err != nil {
			return nil, fmt.Errorf("invalid repeat expression for sequence %q: %s", seq.ID, err.Error())
		}

		result, err := expr.Evaluate(d.ctx)
		if err != nil {
			return nil, fmt.Errorf("invalid repeat expression for sequence %q: %s", seq.ID, err.Error())
		}

		rem = cast.ToInt(result)
	}

	for rem != 0 {
		val, err := d.readType(seq, typ, true, base...)
		if err != nil {
			return nil, err
		}
		rval = append(rval, val)

		rem = rem - 1
	}

	return rval, nil
}

func (d *decodeContext) readSequenceArray(seq *Sequence, base ...string) (interface{}, error) {
	rval := make([]interface{}, 0)
	rem := -1

	if seq.RepeatExpression != nil && *seq.Repeat == "expr" {
		expr, err := govaluate.NewEvaluableExpressionWithResolver(*seq.RepeatExpression, d)
		if err != nil {
			return nil, fmt.Errorf("invalid repeat expression for sequence %q: %s", seq.ID, err.Error())
		}

		result, err := expr.Evaluate(d.ctx)
		if err != nil {
			return nil, fmt.Errorf("invalid repeat expression for sequence %q: %s", seq.ID, err.Error())
		}

		rem = cast.ToInt(result)
	}

	tmp := *seq
	tmp.Repeat = nil

	for rem != 0 {
		val, err := d.readSequence(&tmp, base...)
		if err != nil {
			return nil, err
		}
		rval = append(rval, val)

		rem = rem - 1
	}

	return rval, nil
}

func (d *decodeContext) readInstance(inst *Instance) (interface{}, error) {
	if inst.Pos != nil {
		pos, err := inst.evalPos(d)
		if err != nil {
			return nil, err
		}
		data := d.data[pos:]
		sd := &decodeContext{
			Spec:      d.Spec,
			evalFuncs: d.evalFuncs,
			ctx:       d.ctx,
			reader:    bitreader.NewReader(bytes.NewReader(data)),
			data:      data,
		}

		val, err := sd.readSequence(&inst.Sequence)
		if err != nil {
			return nil, err
		}

		d.ctx.Set(inst.ID, val)
	} else if inst.Value != nil {
		val, err := inst.evalValue(d)
		if err != nil {
			return nil, fmt.Errorf("invalid value expression for instance %q: %s", inst.ID, err.Error())
		}

		d.ctx.Set(inst.ID, val)
	}

	return d.ctx[inst.ID], nil
}

func (d *decodeContext) Resolve(token string) (govaluate.ExpressionFunction, bool) {
	if fn, ok := d.evalFuncs[token]; ok {
		return fn, true
	}
	if t, err := d.vm.Get(token); err == nil {
		if t.IsFunction() {
			fn := func(ctx types.StringMap, args ...interface{}) (interface{}, error) {
				val, err := t.Call(otto.NullValue(), args...)
				if err != nil {
					return nil, err
				}
				if val.IsNull() {
					return nil, nil
				}
				v, err := val.Export()
				if err != nil {
					return nil, err
				}
				return v, nil
			}
			cacheLock.Lock()
			evalFnCache.Add(d.Spec.Meta.ID+token, fn)
			cacheLock.Unlock()

			d.evalFuncs[token] = func(args ...interface{}) (interface{}, error) {
				return fn(d.ctx, args...)
			}
			return d.evalFuncs[token], true
		}
	}

	return nil, false
}

// UnmarshalYAML supports a custom yaml unmarshaller
func (s *Spec) UnmarshalYAML(unmarshal func(interface{}) error) error {
	tmp := struct {
		Meta      Meta             `yaml:"meta"`
		Sequence  []*Sequence      `yaml:"seq"`
		Instances yaml.MapSlice    `yaml:"instances"`
		Types     map[string]*Type `yaml:"types,omitempty"`
	}{}

	if err := unmarshal(&tmp); err != nil {
		return err
	}

	s.Meta = tmp.Meta
	s.Sequence = tmp.Sequence
	s.Types = tmp.Types
	s.Instances = make([]*Instance, 0)

	for _, entry := range tmp.Instances {
		inst := &Instance{}
		parts := make(map[string]interface{})

		if sl, ok := entry.Value.(yaml.MapSlice); ok {
			for _, p := range sl {
				parts[cast.ToString(p.Key)] = p.Value
			}
		}

		util.MapStruct(inst, parts)
		inst.ID = cast.ToString(entry.Key)

		s.Instances = append(s.Instances, inst)
	}

	return nil
}

// UnmarshalYAML supports a custom yaml unmarshaller
func (t *Type) UnmarshalYAML(unmarshal func(interface{}) error) error {
	tmp := struct {
		Sequence  []*Sequence   `yaml:"seq"`
		Instances yaml.MapSlice `yaml:"instances"`
	}{}

	if err := unmarshal(&tmp); err != nil {
		return err
	}

	t.Sequence = tmp.Sequence
	t.Instances = make([]*Instance, 0)

	for _, entry := range tmp.Instances {
		inst := &Instance{}
		parts := make(map[string]interface{})

		if sl, ok := entry.Value.(yaml.MapSlice); ok {
			for _, p := range sl {
				parts[cast.ToString(p.Key)] = p.Value
			}
		}

		util.MapStruct(inst, parts)
		inst.ID = cast.ToString(entry.Key)

		t.Instances = append(t.Instances, inst)
	}

	return nil
}

func parseTagSetting(tags reflect.StructTag) map[string]string {
	setting := map[string]string{}
	for _, str := range []string{tags.Get("binary")} {
		if str == "" {
			continue
		}
		tags := strings.Split(str, ";")
		for _, value := range tags {
			v := strings.Split(value, ":")
			k := strings.TrimSpace(strings.ToUpper(v[0]))
			if len(v) >= 2 {
				setting[k] = strings.Join(v[1:], ":")
			} else {
				setting[k] = k
			}
		}
	}
	return setting
}
