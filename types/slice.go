/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"reflect"

	"github.com/spf13/cast"
)

// Slicer is a generic slice manipulation interface helper
type Slicer interface {
	Append(vals ...interface{}) Slicer
	Insert(index int, val interface{}) Slicer
	IndexOf(val interface{}) int
	Len() int
	Get(index int) interface{}
	Remove(index int) Slicer
	Without(vals ...interface{}) Slicer
	Filter(func(index int, val interface{}) bool) Slicer
	ForEach(func(index int, val interface{})) Slicer
	Map(out interface{}, filters ...MapFilter) Slicer
	ToStringMap() StringMap
	Contains(values ...interface{}) bool
	ContainsAny(values ...interface{}) bool
	Find(f func(val interface{}) bool) interface{}
	Slice() []interface{}
}

type slice struct {
	v reflect.Value
}

// MapFilter returns true and the key and value, or false if the value should be skipped
type MapFilter func(index int, val interface{}) (string, interface{}, bool)

// Slice returns a slicer object from a pointer to a slice
// 	v := make([]string, 0)
//	util.Slice(&v).Append("foo")
func Slice(s interface{}) Slicer {
	val := reflect.ValueOf(s)
	if val.Kind() == reflect.Ptr {
		val = val.Elem()
	}
	return &slice{
		v: val,
	}
}

func (s *slice) Append(vals ...interface{}) Slicer {
	for _, val := range vals {
		s.v.Set(reflect.Append(s.v, reflect.ValueOf(val)))
	}

	return s
}

func (s *slice) Insert(index int, val interface{}) Slicer {
	s.v.Set(reflect.AppendSlice(s.v.Slice(0, index+1), s.v.Slice(index, s.v.Len())))
	s.v.Index(index).Set(reflect.ValueOf(val))

	return s
}

func (s *slice) Len() int {
	return s.v.Len()
}

func (s *slice) Remove(index int) Slicer {
	s.v.Set(reflect.AppendSlice(s.v.Slice(0, index), s.v.Slice(index+1, s.v.Len())))
	return s
}

func (s *slice) ForEach(foreach func(index int, val interface{})) Slicer {
	for i := 0; i < s.v.Len(); i++ {
		foreach(i, s.v.Index(i).Interface())
	}

	return s
}

func (s *slice) Without(vals ...interface{}) Slicer {
	if len(vals) > 0 {
		if s, ok := vals[0].(Slicer); ok {
			vals = s.Slice()
		}
	}
	return s.Filter(func(index int, val interface{}) bool {
		for _, v := range vals {
			if reflect.DeepEqual(val, v) {
				return false
			}
		}
		return true
	})
}

func (s *slice) Filter(filter func(index int, val interface{}) bool) Slicer {

	compSlice := reflect.MakeSlice(s.v.Type(), 0, s.v.Len())

	for i := 0; i < s.v.Len(); i++ {
		if filter(i, s.v.Index(i).Interface()) {
			compSlice = reflect.Append(compSlice, s.v.Index(i))
		}
	}

	s.v.Set(compSlice)

	return s
}

// Map maps the slice to the destination using the filters
// Destination can be a slice or a map
func (s *slice) Map(out interface{}, filters ...MapFilter) Slicer {
	rval := reflect.ValueOf(out).Elem()

	for _, filter := range filters {
		for i := 0; i < s.v.Len(); i++ {
			if k, v, skip := filter(i, s.v.Index(i).Interface()); !skip {
				switch rval.Kind() {
				case reflect.Slice:
					Slice(out).Append(v)
				case reflect.Map:
					Map(out).Set(k, v)
				}
			}
		}
	}

	return s
}

func (s *slice) ToStringMap() StringMap {
	rval := make(StringMap)
	for i := 0; i < s.v.Len(); i++ {
		rval[cast.ToString(i)] = s.v.Index(i).Interface()
	}
	return rval
}

func (s *slice) IndexOf(val interface{}) int {
	for i := 0; i < s.v.Len(); i++ {
		if reflect.DeepEqual(s.v.Index(i).Interface(), val) {
			return i
		}
	}

	return -1
}

func (s *slice) Get(index int) interface{} {
	if index >= s.v.Len() {
		return nil
	}

	val := s.v.Index(index)
	if !val.CanInterface() || val.IsNil() {
		return nil
	}
	return val.Interface()
}

func (s *slice) Contains(values ...interface{}) bool {
	for _, val := range values {
		if vs, ok := val.(Slicer); ok {
			if !s.Contains(vs.Slice()...) {
				return false
			}
		} else if s.IndexOf(val) < 0 {
			return false
		}
	}
	return true
}

func (s *slice) ContainsAny(values ...interface{}) bool {
	for _, val := range values {
		if vs, ok := val.(Slicer); ok {
			if s.ContainsAny(vs.Slice()...) {
				return true
			}
		} else if s.IndexOf(val) >= 0 {
			return true
		}
	}
	return false
}

func (s *slice) Find(f func(val interface{}) bool) interface{} {
	for i := 0; i < s.v.Len(); i++ {
		if f(s.v.Index(i).Interface()) {
			return s.v.Index(i).Interface()
		}
	}
	return nil
}

func (s *slice) Slice() []interface{} {
	out := make([]interface{}, 0)
	for i := 0; i < s.v.Len(); i++ {
		out = append(out, s.v.Index(i).Interface())
	}
	return out
}
