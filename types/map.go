/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"reflect"

	"github.com/spf13/cast"
)

// Mapper provides a map wrapper interface for working easily with unknown map types
type Mapper interface {
	IsSet(key interface{}) bool
	Set(key, value interface{})
	Get(key interface{}) interface{}
	Cast(key interface{}) Caster
	Sub(key interface{}) Mapper
	Delete(key interface{})
	DeleteAll(keys ...interface{})
	Copy() Mapper
	With(keys ...interface{}) Mapper
	Without(keys ...interface{}) Mapper
	Values() interface{}
	Keys() interface{}
	StringMap() StringMap
	ForEach(func(key, value interface{}))
}

type genericMap struct {
	v reflect.Value
}

// Map returns a Mapper from the passed map
func Map(m interface{}) Mapper {
	switch t := m.(type) {
	case map[string]interface{}:
		return StringMap(t)
	}

	v := reflect.ValueOf(m)
	if v.Kind() == reflect.Ptr {
		v = v.Elem()
	}
	if v.Kind() != reflect.Map {
		panic("not a map")
	}

	return &genericMap{v: v}
}

func (m genericMap) IsSet(key interface{}) bool {
	return m.v.MapIndex(reflect.ValueOf(key)).IsValid()
}

// Set sets a value in the map
func (m genericMap) Set(key, value interface{}) {
	m.v.SetMapIndex(reflect.ValueOf(key), reflect.ValueOf(value))
}

// Keys returns the keys from the map
func (m genericMap) Keys() interface{} {
	keySlice := reflect.SliceOf(m.v.Type().Key())
	keys := reflect.MakeSlice(keySlice, m.v.Len(), m.v.Len())

	for i, key := range m.v.MapKeys() {
		idx := keys.Index(i)
		idx.Set(key)
	}
	return keys.Interface()
}

// Values returns the values from the map
func (m genericMap) Values() interface{} {
	valSlice := reflect.SliceOf(m.v.Type().Elem())
	values := reflect.MakeSlice(valSlice, m.v.Len(), m.v.Len())

	for i, key := range m.v.MapKeys() {
		val := m.v.MapIndex(key)
		idx := values.Index(i)
		idx.Set(val)
	}
	return values.Interface()
}

// Get returns a value from a map
func (m genericMap) Get(key interface{}) interface{} {
	v := m.v.MapIndex(reflect.ValueOf(key))
	if !v.CanInterface() {
		return nil
	}
	return v.Interface()
}

// Sub returns a sub map
func (m genericMap) Sub(key interface{}) Mapper {
	v := m.v.MapIndex(reflect.ValueOf(key))
	if !v.CanInterface() {
		return nil
	}
	if v.Kind() != reflect.Map {
		return nil
	}
	return Map(v.Interface())
}

// Delete removes a value from a map
func (m genericMap) Delete(key interface{}) {
	m.v.SetMapIndex(reflect.ValueOf(key), reflect.Value{})
}

// DeleteAll removes a value from a map
func (m genericMap) DeleteAll(keys ...interface{}) {
	for _, key := range keys {
		m.v.SetMapIndex(reflect.ValueOf(key), reflect.Value{})
	}
}

// Copy returns a copy of a map
func (m genericMap) Copy() Mapper {
	dst := reflect.New(reflect.TypeOf(m.v))

	for _, k := range m.v.MapKeys() {
		ov := m.v.MapIndex(k)
		dst.SetMapIndex(k, ov)
	}
	return &genericMap{dst}
}

// Cast returns a Caster to convert a value to another type
func (m genericMap) Cast(key interface{}) Caster {
	v := m.v.MapIndex(reflect.ValueOf(key))
	if !v.CanInterface() {
		return NewValue(nil)
	}
	return NewValue(v.Interface())
}

// Without returns a copy of the map without the specified keys
func (m genericMap) Without(keys ...interface{}) Mapper {
	dst := m.Copy()

	for _, key := range keys {
		dst.Delete(key)
	}

	return dst
}

// With returns a copy of the map with the specified keys
func (m genericMap) With(keys ...interface{}) Mapper {
	dst := reflect.New(reflect.TypeOf(m.v))

	for _, key := range keys {
		dst.SetMapIndex(reflect.ValueOf(key), reflect.ValueOf(m.Get(key)))
	}

	return Map(dst.Interface())
}

func (m genericMap) StringMap() StringMap {
	rval := make(StringMap)

	m.ForEach(func(k, v interface{}) {
		rval[cast.ToString(k)] = v
	})

	return rval
}

func (m genericMap) ForEach(itr func(key, value interface{})) {
	for _, key := range m.v.MapKeys() {
		itr(key, m.Get(key))
	}
}
