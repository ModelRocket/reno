/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"context"
	"encoding/json"

	strfmt "github.com/go-openapi/strfmt"
	"github.com/gofrs/uuid"
	"github.com/lib/pq"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/swag"
)

// ModelGenerateUUID is a flag to tell sparks to generate a new uuid for the models
var ModelGenerateUUID = false

// Model is the default database model that uses a UUID as a primary key
type Model struct {
	ModelBase
	// Universally unique identifier
	// Read Only: true
	ID strfmt.UUID `json:"id,omitempty"`
}

// CompoundModel uses an integer for the primary key, but also has an optional UUID
type CompoundModel struct {
	ModelBase
	// Table unitque identifier
	// Read Only: true
	ID int64 `json:"id,omitempty"`

	// Universally unique identifier
	// Read Only: true
	UUID *strfmt.UUID `json:"uuid,omitempty"`
}

// ModelBase is the base model attributes
type ModelBase struct {
	// Date and time of object creation
	// Read Only: true
	CreatedAt *strfmt.DateTime `json:"created_at,omitempty" sql:"column:_created_at"`

	// Last date of object modification
	// Read Only: true
	UpdatedAt *strfmt.DateTime `json:"updated_at,omitempty" sql:"column:_updated_at"`

	// The date of object soft delete
	// Read Only: true
	DeletedAt *strfmt.DateTime `json:"deleted_at,omitempty" sql:"column:_deleted_at"`

	// CreatedBy opaque identifier for created by
	CreatedBy interface{} `json:"created_by,omitempty" sql:"column:_created_by"`

	// UpdatedBy opaque identifier for updated by
	UpdatedBy interface{} `json:"updated_by,omitempty" sql:"column:_updated_by"`

	// attributes
	Attributes StringMap `json:"attributes,omitempty" sql:"column:meta"`

	// tags
	Tags pq.StringArray `json:"tags,omitempty"`

	// the context for the model
	context context.Context
}

// MarshalJSON marshals this object with a polymorphic type to a JSON structure
func (m Model) MarshalJSON() ([]byte, error) {
	var b1, b2 []byte
	var err error
	b1, err = json.Marshal(m.ModelBase)
	if err != nil {
		return nil, err
	}

	b2, err = json.Marshal(struct {
		ID strfmt.UUID `json:"id,omitempty"`
	}{
		ID: m.ID,
	},
	)
	if err != nil {
		return nil, err
	}

	return swag.ConcatJSON(b1, b2), nil
}

// MarshalJSON marshals this object with a polymorphic type to a JSON structure
func (m ModelBase) MarshalJSON() ([]byte, error) {
	var b1, b2 []byte
	var err error
	b1, err = json.Marshal(struct {
		// Date and time of object creation
		// Read Only: true
		CreatedAt *strfmt.DateTime `json:"created_at,omitempty" sql:"column:_created_at"`

		// Last date of object modification
		// Read Only: true
		UpdatedAt *strfmt.DateTime `json:"updated_at,omitempty" sql:"column:_updated_at"`

		// The date of object soft delete
		// Read Only: true
		DeletedAt *strfmt.DateTime `json:"deleted_at,omitempty" sql:"column:_deleted_at"`

		// CreatedBy opaque identifier for created by
		CreatedBy interface{} `json:"created_by,omitempty" sql:"column:_created_by"`

		// UpdatedBy opaque identifier for updated by
		UpdatedBy interface{} `json:"updated_by,omitempty" sql:"column:_updated_by"`

		// meta
		Meta StringMap `json:"meta,omitempty"`

		// tags
		Tags pq.StringArray `json:"tags,omitempty"`
	}{

		CreatedAt: m.CreatedAt,

		UpdatedAt: m.UpdatedAt,

		DeletedAt: m.DeletedAt,

		Meta: m.Attributes,

		Tags: m.Tags,
	},
	)
	if err != nil {
		return nil, err
	}

	return swag.ConcatJSON(b1, b2), nil
}

// Validate validates this base model
func (m *Model) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *Model) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *Model) UnmarshalBinary(b []byte) error {
	var res Model
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// BeforeCreate hooks the gorm on create
func (m *Model) BeforeCreate() (err error) {
	if ModelGenerateUUID {
		u, err := uuid.NewV4()
		if err != nil {
			return err
		}
		m.ID = strfmt.UUID(u.String())
	}
	return nil
}

// Validate validates this base model
func (m *CompoundModel) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *CompoundModel) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *CompoundModel) UnmarshalBinary(b []byte) error {
	var res CompoundModel
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// BeforeCreate hooks the gorm on create
func (m *CompoundModel) BeforeCreate() (err error) {
	if ModelGenerateUUID {
		u, err := uuid.NewV4()
		if err != nil {
			return err
		}
		tmp := strfmt.UUID(u.String())
		m.UUID = &tmp
	}
	return nil
}

// Validate validates this base model
func (m *ModelBase) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// Context returns the context for the model instance
func (m *ModelBase) Context() context.Context {
	if m.context == nil {
		m.context = context.Background()
	}
	return m.context
}

// SetContext sets a context key on the object
func (m *ModelBase) SetContext(key, value interface{}) context.Context {
	if m.context == nil {
		m.context = context.Background()
	}

	m.context = context.WithValue(m.context, key, value)

	return m.context
}
