/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"encoding/json"
	"strings"
	"time"

	"github.com/influxdata/influxdb/models"
	"gitlab.com/ModelRocket/reno/util"
)

type (
	// InfluxSeries is a class for scanning influx data
	InfluxSeries struct {
		cols   []string
		colmap map[string]int
		tags   map[string]string
		vals   [][]interface{}
	}

	// InfluxPoint is a point in series data
	InfluxPoint struct {
		s      *InfluxSeries
		Values []interface{}
		Index  int
		Time   time.Time
	}
)

// NewInfluxSeries creates a new influx series mapper
func NewInfluxSeries(row models.Row, colPrefix ...string) *InfluxSeries {
	s := &InfluxSeries{
		cols:   row.Columns,
		tags:   row.Tags,
		colmap: make(map[string]int),
		vals:   row.Values,
	}

	var prefix string
	if len(colPrefix) > 0 {
		prefix = colPrefix[0] + "_"
	}
	for i, c := range row.Columns {
		s.colmap[strings.TrimPrefix(c, prefix)] = i
	}

	return s
}

// Tags returns the list of tags
func (s *InfluxSeries) Tags() map[string]string {
	return s.tags
}

// Tag returns a tag value
func (s *InfluxSeries) Tag(key string) string {
	return s.tags[key]
}

// Scan a series
func (s *InfluxSeries) Scan(scan func(point *InfluxPoint) error) error {
	for i, v := range s.vals {
		ts := NewValue(v[s.ColIndex("time")])
		if err := scan(&InfluxPoint{s: s, Values: v, Index: i, Time: ts.Time()}); err != nil {
			return err
		}
	}
	return nil
}

// ColIndex get the index of a column
func (s *InfluxSeries) ColIndex(key string) int {
	if i, ok := s.colmap[key]; ok {
		return i
	}
	return -1
}

// AddColumn adds a new column to the series at the end
func (s *InfluxSeries) AddColumn(key string) int {
	if i := s.ColIndex(key); i >= 0 {
		return i
	}

	s.cols = append(s.cols, key)
	s.colmap[key] = len(s.cols) - 1

	return s.colmap[key]
}

// NewPoint creates a new point from the map
func (s *InfluxSeries) NewPoint(val interface{}, ts time.Time, points ...*InfluxPoint) *InfluxPoint {
	var row []interface{}

	switch tval := val.(type) {
	case []interface{}:
		row = tval
	case map[string]interface{}:
		row = make([]interface{}, len(s.colmap))

		for _, p := range points {
			for _, k := range p.Columns() {
				if i := s.ColIndex(k); i >= 0 {
					row[i] = p.Get(k)
				} else {
					s.AddColumn(k)
					row = append(row, p.Get(k))
				}
			}
		}

		for k, v := range tval {
			if i := s.ColIndex(k); i >= 0 {
				row[i] = v
			} else {
				s.AddColumn(k)
				row = append(row, v)
			}
		}
	}

	s.vals = append(s.vals, row)

	return &InfluxPoint{s: s, Values: row, Index: len(s.vals) - 1, Time: ts}
}

// Columns return the point columns
func (s *InfluxSeries) Columns() []string {
	return s.cols
}

// Length returns the length of the values
func (s *InfluxSeries) Length() int {
	return len(s.vals)
}

// ColIndex get the index of a column
func (p *InfluxPoint) ColIndex(key string) int {
	return p.s.ColIndex(key)
}

// Columns return the point columns
func (p *InfluxPoint) Columns() []string {
	return p.s.cols
}

// Tags returns the list of tags
func (p *InfluxPoint) Tags() map[string]string {
	return p.s.tags
}

// Series returns the series for the point
func (p *InfluxPoint) Series() *InfluxSeries {
	return p.s
}

// Map returns a map of the point
func (p *InfluxPoint) Map() map[string]interface{} {
	rval := make(map[string]interface{})

	// add the tags
	for k, v := range p.s.tags {
		rval[k] = v
	}

	// add the fields
	for k := range p.s.colmap {
		if v := p.Value(k).Interface(); v != nil {
			rval[k] = v
		}
	}
	return rval
}

// Get returns a value for the key from values or tags
func (p *InfluxPoint) Get(key string) Value {
	i := p.ColIndex(key)
	if i >= 0 && i < len(p.Values) {
		return NewValue(p.Values[i])
	}
	return NewValue(p.Tag(key))
}

// Tag returns a tag value
func (p *InfluxPoint) Tag(key string) string {
	return p.s.tags[key]
}

// Value returns a value for the key from values or tags
func (p *InfluxPoint) Value(key string) Value {
	i := p.ColIndex(key)
	if i >= 0 && i < len(p.Values) {
		return NewValue(p.Values[i])
	}
	return NewValue(nil)
}

// UnmarshalStruct unmarshals series data into the structure
func (p *InfluxPoint) UnmarshalStruct(prefix string, out interface{}, tags ...string) bool {
	valMap := make(map[string]interface{})

	if prefix != "" {
		prefix = prefix + "_"
	}

	for col, i := range p.s.colmap {
		if strings.HasPrefix(col, prefix) {
			switch t := p.Values[i].(type) {
			case json.Number:
				if tmp, err := t.Float64(); err == nil {
					valMap[strings.TrimPrefix(col, prefix)] = tmp
				}
			default:
				valMap[strings.TrimPrefix(col, prefix)] = t
			}
		}
	}

	valMap = util.NormalizeMap(valMap, true)

	if len(valMap) == 0 {
		return false
	}

	keys, _ := util.MapStructKeys(out, valMap, tags...)
	if len(keys) == 0 {
		return false
	}

	return true
}
