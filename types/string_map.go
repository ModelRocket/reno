/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"reflect"
	"strings"
	"time"

	"github.com/go-openapi/strfmt"
	"github.com/kr/pretty"
	"github.com/spf13/cast"
)

// StringMap is a wrapper on a map[string]interface{} that implements the Mapper interface
type StringMap map[string]interface{}

// StringMapArray represents an array of string maps
type StringMapArray []StringMap

var (
	// PathSeparator is the character used to separate the elements
	// of the keypath.
	//
	// For example, `location.address.city`
	PathSeparator = "."

	// SignatureSeparator is the character that is used to
	// separate the Base64 string from the security signature.
	SignatureSeparator = "_"
)

// IsSet returns true if the parameter is set
func (p StringMap) IsSet(key interface{}) bool {
	_, ok := p[cast.ToString(key)]
	return ok
}

// Set sets a value in the map
func (p StringMap) Set(key, value interface{}) {
	keypath := cast.ToString(key)

	segs := strings.Split(keypath, PathSeparator)

	if len(segs) == 1 {
		p[segs[0]] = value
		return
	}

	switch val := p[segs[0]].(type) {
	case StringMap:
		val.Set(strings.Join(segs[1:], "."), value)

	case *StringMap:
		val.Set(strings.Join(segs[1:], "."), value)

	case map[string]interface{}:
		StringMap(val).Set(strings.Join(segs[1:], "."), value)

	default:
		refVal := reflect.ValueOf(val)
		switch refVal.Kind() {
		case reflect.Invalid:
			if index, err := cast.ToIntE(segs[1]); err == nil {
				tmp := make([]*StringMap, index+1)
				tmp[index] = &StringMap{segs[2]: value}
				p[segs[0]] = tmp
			} else if segs[1] == "$" {
				tmp := make([]StringMap, 0)

				switch subVal := value.(type) {
				case []StringMap:
					for _, v := range subVal {
						tmp = append(tmp, v)
					}
				case []*StringMap:
					for _, v := range subVal {
						tmp = append(tmp, *v)
					}
				case []map[string]interface{}:
					for _, v := range subVal {
						smap := StringMap(v)
						tmp = append(tmp, smap)
					}
				}
				p[segs[0]] = tmp
			} else {
				p[segs[0]] = make(StringMap)
				StringMap(p[segs[0]].(StringMap)).Set(strings.Join(segs[1:], "."), value)
			}

		case reflect.Slice:
			if tmp, ok := val.([]StringMap); ok {
				switch subVal := value.(type) {
				case []StringMap:
					for i, v := range subVal {
						for k, v := range v {
							tmp[i].Set(k, v)
						}
					}
				case []*StringMap:
					for i, v := range subVal {
						for k, v := range *v {
							tmp[i].Set(k, v)
						}
					}
				case []map[string]interface{}:
					for i, v := range subVal {
						for k, v := range v {
							tmp[i].Set(k, v)
						}
					}
				}
			}

		default:
			pretty.Log("error default")
		}
	}
}

// Get returns the value and if the key is set
func (p StringMap) Get(key interface{}) interface{} {
	keypath := cast.ToString(key)

	var segs = strings.Split(keypath, PathSeparator)

	if len(segs) == 1 {
		return p[segs[0]]
	}

	switch val := p[segs[0]].(type) {
	case StringMap:
		return val.Get(strings.Join(segs[1:], "."))

	case *StringMap:
		return val.Get(strings.Join(segs[1:], "."))

	case map[string]interface{}:
		return StringMap(val).Get(strings.Join(segs[1:], "."))

	default:
		refVal := reflect.ValueOf(val)
		if refVal.Kind() == reflect.Slice {
			if segs[1] == "$" {
				if len(segs[2:]) < 1 {
					return nil
				}

				rval := make([]StringMap, 0)
				for i := 0; i < refVal.Len(); i++ {
					val := refVal.Index(i).Interface()

					if len(segs[2:]) < 1 {
						rval = append(rval, StringMap{segs[2]: val})
						continue
					}

					switch vv := val.(type) {
					case StringMap:
						rval = append(rval, StringMap{segs[2]: vv.Get(strings.Join(segs[2:], "."))})

					case *StringMap:
						rval = append(rval, StringMap{segs[2]: vv.Get(strings.Join(segs[2:], "."))})

					case map[string]interface{}:
						rval = append(rval, StringMap{segs[2]: StringMap(vv).Get(strings.Join(segs[2:], "."))})

					default:
						rval = append(rval, StringMap{segs[2]: vv})
					}
				}
				return rval
			}

			index := cast.ToInt(segs[1])
			if index < refVal.Len() {
				ival := refVal.Index(index).Interface()

				if len(segs[2:]) < 1 {
					return ival
				}

				switch vv := ival.(type) {
				case StringMap:
					return vv.Get(strings.Join(segs[2:], "."))

				case *StringMap:
					return vv.Get(strings.Join(segs[2:], "."))

				case map[string]interface{}:
					return StringMap(vv).Get(strings.Join(segs[2:], "."))

				default:
					return vv
				}
			} else {
				return nil
			}
		}

		return val
	}
}

// Sub returns a sub StringMap for the key
func (p StringMap) Sub(key interface{}) Mapper {
	if tmp := p.Get(key); tmp != nil {
		switch p := tmp.(type) {
		case map[string]interface{}:
			return StringMap(p)
		default:
			return Map(p)
		}
	}
	return StringMap{}
}

// Delete removes a key
func (p StringMap) Delete(key interface{}) {
	keypath := cast.ToString(key)

	var segs = strings.Split(keypath, PathSeparator)

	if len(segs) == 1 {
		delete(p, segs[0])
		return
	}

	switch val := p[segs[0]].(type) {
	case StringMap:
		val.Delete(strings.Join(segs[1:], "."))

	case *StringMap:
		val.Delete(strings.Join(segs[1:], "."))

	case map[string]interface{}:
		StringMap(val).Delete(strings.Join(segs[1:], "."))

	default:
		refVal := reflect.ValueOf(val)
		if refVal.Kind() == reflect.Slice {
			Slice(val).ToStringMap().Delete(strings.Join(segs[1:], "."))
		}
	}
}

// DeleteAll removes several keys
func (p StringMap) DeleteAll(keys ...interface{}) {
	for _, key := range keys {
		delete(p, cast.ToString(key))
	}
}

// Copy does a shallow copy
func (p StringMap) Copy() Mapper {
	rval := make(StringMap)
	for k, v := range p {
		rval[k] = v
	}
	return rval
}

// Without removes the keys, returns a shallow copy
func (p StringMap) Without(keys ...interface{}) Mapper {
	if len(keys) == 0 {
		return p
	}
	rval := p.Copy().(StringMap)
	for _, key := range keys {
		rval.Delete(key)
	}
	return rval
}

// With returns a map with only the specified keys
func (p StringMap) With(keys ...interface{}) Mapper {
	if len(keys) == 0 {
		return p
	}

	rval := make(StringMap)
	for _, key := range keys {
		rval.Set(key, p.Get(key))
	}
	return rval
}

// StringMap returns the string map
func (p StringMap) StringMap() StringMap {
	return p
}

// ForEach iterates over the map
func (p StringMap) ForEach(itr func(key, value interface{})) {
	for k, v := range p {
		itr(k, v)
	}
}

// GetValue returns a Value and if the key is set
func (p StringMap) GetValue(key string) (Value, bool) {
	v, ok := p[key]
	return NewValue(v), ok
}

// Cast returns Caster to convert the value to another type
func (p StringMap) Cast(key interface{}) Caster {
	return NewValue(p[cast.ToString(key)])
}

// String returns a string value for the param, or the optional default
func (p StringMap) String(key string, def ...string) string {
	rval := p.Get(key)
	if rval == nil {
		if len(def) > 0 {
			return def[0]
		}
		return ""
	}

	return cast.ToString(rval)
}

// StringPtr returns a string ptr or nil
func (p StringMap) StringPtr(key string, def ...string) *string {
	rval := p.Get(key)
	if rval == nil {
		if len(def) > 0 {
			return &def[0]
		}
		return nil
	}
	tmp := cast.ToString(rval)
	return &tmp
}

// StringSlice returns a string value for the param, or the optional default
func (p StringMap) StringSlice(key string) []string {
	rval := p.Get(key)
	if rval == nil {
		return []string{}
	}

	return cast.ToStringSlice(rval)
}

// Bool parses and returns the boolean value of the parameter
func (p StringMap) Bool(key string, def ...bool) bool {
	rval := p.Get(key)
	if rval == nil {
		if len(def) > 0 {
			return def[0]
		}
		return false
	}

	return cast.ToBool(rval)
}

// Int64 returns the int value or 0 if not set
func (p StringMap) Int64(key string, def ...int64) int64 {
	rval := p.Get(key)
	if rval == nil {
		if len(def) > 0 {
			return def[0]
		}
		return 0
	}
	if val, ok := rval.(json.Number); ok {
		if rval, err := val.Int64(); err == nil {
			return rval
		}
	}
	return cast.ToInt64(rval)
}

// Int returns the int value or 0 if not set
func (p StringMap) Int(key string, def ...int) int {
	rval := p.Get(key)
	if rval == nil {
		if len(def) > 0 {
			return def[0]
		}
		return 0
	}
	if val, ok := rval.(json.Number); ok {
		if rval, err := val.Int64(); err == nil {
			return int(rval)
		}
	}
	return cast.ToInt(rval)
}

// Duration returns a duration value
func (p StringMap) Duration(key string, def ...time.Duration) time.Duration {
	rval := p.Get(key)
	if rval == nil {
		if len(def) > 0 {
			return def[0]
		}
		return 0
	}
	if val, ok := rval.(json.Number); ok {
		if tmp, err := val.Int64(); err == nil {
			rval = tmp
		}
	}
	return cast.ToDuration(rval)
}

// Float64 returns the float value or 0 if not set
func (p StringMap) Float64(key string, def ...float64) float64 {
	rval := p.Get(key)
	if rval == nil {
		if len(def) > 0 {
			return def[0]
		}
		return 0
	}
	if val, ok := rval.(json.Number); ok {
		if rval, err := val.Float64(); err == nil {
			return rval
		}
	}
	return cast.ToFloat64(rval)
}

// Keys returns the keys from the map
func (p StringMap) Keys() interface{} {
	keys := make([]string, 0)
	for k := range p {
		keys = append(keys, k)
	}
	return keys
}

// Values returns the keys from the map
func (p StringMap) Values() interface{} {
	vals := make([]interface{}, 0)
	for _, v := range p {
		vals = append(vals, v)
	}
	return vals
}

// Validate handles the strfmt validation for the StringArray object
func (*StringMap) Validate(formats strfmt.Registry) error {
	return nil
}

// Scan implements the sql.Scanner interface
func (p *StringMap) Scan(src interface{}) error {
	bytes, ok := src.([]byte)
	if !ok {
		return fmt.Errorf("Invalid data type for StringMap")
	}
	return json.Unmarshal(bytes, p)
}

// Value implements the driver.Valuer interface
func (p StringMap) Value() (driver.Value, error) {
	data, err := json.Marshal(map[string]interface{}(p))
	if err != nil {
		return nil, err
	}
	return string(data), nil
}

// Validate handles the strfmt validation for the StringArray object
func (*StringMapArray) Validate(formats strfmt.Registry) error {
	return nil
}

// Scan implements the sql.Scanner interface
func (p *StringMapArray) Scan(src interface{}) error {
	bytes, ok := src.([]byte)
	if !ok {
		return fmt.Errorf("Invalid data type for StringMapArray")
	}
	return json.Unmarshal(bytes, p)
}

// Value implements the driver.Valuer interface
func (p StringMapArray) Value() (driver.Value, error) {
	data, err := json.Marshal(p)
	if err != nil {
		return nil, err
	}
	return string(data), nil
}
