/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package openid

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"

	"github.com/go-openapi/errors"
	"github.com/go-openapi/strfmt"
	"github.com/go-openapi/swag"
	"gitlab.com/ModelRocket/reno/types/ptr"
)

// AddressClaim End-User's preferred postal address. The value of the address member is a JSON [RFC4627] structure containing some or all of the members defined in Section 5.1.1.
// swagger:model AddressClaim
type AddressClaim struct {

	// Country name component.
	Country *string `json:"country,omitempty"`

	// Full mailing address, formatted for display or use on a mailing label. This field MAY contain multiple lines, separated by newlines. Newlines can be represented either as a carriage return/line feed pair ("\r\n") or as a single line feed character ("\n").
	Formatted *string `json:"formatted,omitempty"`

	// City or locality component.
	Locality *string `json:"locality,omitempty"`

	// Zip code or postal code component.
	PostalCode *string `json:"postal_code,omitempty"`

	// State, province, prefecture, or region component.
	Region *string `json:"region,omitempty"`

	// Full street address component, which MAY include house number, street name, Post Office Box, and multi-line extended street address information. This field MAY contain multiple lines, separated by newlines. Newlines can be represented either as a carriage return/line feed pair ("\r\n") or as a single line feed character ("\n").
	StreetAddress *string `json:"street_address,omitempty"`
}

// FormattedAddress returns the fully formated address
func (m *AddressClaim) FormattedAddress() string {
	if m == nil {
		return ""
	}
	formatted := ""
	if m.StreetAddress != nil {
		formatted += *m.StreetAddress + "\r\n"
	}
	locality := ""
	if m.Locality != nil {
		locality += *m.Locality + " "
	}
	if m.Region != nil {
		locality += *m.Region + " "
	}
	if m.PostalCode != nil {
		locality += *m.PostalCode
	}
	if locality != "" {
		formatted += locality + "\r\n"
	}
	if m.Country != nil {
		formatted += *m.Country
	}
	return formatted
}

// Validate validates this address claim
func (m *AddressClaim) Validate(formats strfmt.Registry) error {
	var res []error

	if len(res) > 0 {
		return errors.CompositeValidationError(res...)
	}
	return nil
}

// MarshalBinary interface implementation
func (m *AddressClaim) MarshalBinary() ([]byte, error) {
	if m == nil {
		return nil, nil
	}
	return swag.WriteJSON(m)
}

// UnmarshalBinary interface implementation
func (m *AddressClaim) UnmarshalBinary(b []byte) error {
	var res AddressClaim
	if err := swag.ReadJSON(b, &res); err != nil {
		return err
	}
	*m = res
	return nil
}

// Scan implements the sql.Scanner interface
func (m *AddressClaim) Scan(value interface{}) error {
	if m == nil {
		return nil
	}
	bytes, ok := value.([]byte)
	if !ok {
		return fmt.Errorf("Failed to unmarshal JSONB value: %#v", value)
	}
	if err := json.Unmarshal(bytes, m); err != nil {
		return err
	}
	tmp := m.FormattedAddress()
	if len(tmp) > 0 {
		m.Formatted = ptr.String(tmp)
	}
	return nil
}

// Value implements the driver.Valuer interface
func (m AddressClaim) Value() (driver.Value, error) {
	// Generate the formatted address
	tmp := m.FormattedAddress()
	if len(tmp) > 0 {
		m.Formatted = ptr.String(tmp)
	} else {
		m.Formatted = nil
	}

	bytes, err := json.Marshal(m)
	if err != nil {
		return nil, err
	}
	return string(bytes), nil
}
