/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"encoding/base64"
	"encoding/json"
	"net/http"
)

// EncodeCookie returns marshals to json (non-[]byte values) and base64 encodes them using base64.RawURLEncoding
func EncodeCookie(name string, val interface{}) *http.Cookie {
	c := &http.Cookie{
		Name: name,
	}

	switch t := val.(type) {
	case []byte:
		c.Value = base64.RawURLEncoding.EncodeToString(t)
	default:
		data, err := json.Marshal(val)
		if err != nil {
			panic(err)
		}
		c.Value = base64.RawURLEncoding.EncodeToString(data)
	}

	return c
}

// DecodeCookie decodes a cookies
func DecodeCookie(c *http.Cookie, out interface{}) error {
	val, err := base64.RawURLEncoding.DecodeString(c.Value)
	if err != nil {
		return err
	}

	switch t := out.(type) {
	case *[]byte:
		*t = val
	default:
		return json.Unmarshal(val, out)
	}

	return nil
}
