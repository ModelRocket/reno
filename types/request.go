/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"errors"
	"net/http"
)

type (
	// Request defines a simple wrapper around an http.Request with some helpers
	Request struct {
		*http.Request
	}
)

var (
	// ErrCookieNotFound is returned if a cookie value is not found
	ErrCookieNotFound = errors.New("cookie does not exist")
)

// NewRequest returns a new request object
func NewRequest(r *http.Request) *Request {
	return &Request{Request: r}
}

// GetCookie returns a cookie in the request or an empty cookie with the name
func (r *Request) GetCookie(name string) *http.Cookie {
	for _, c := range r.Cookies() {
		if c.Name == name {
			return c
		}
	}

	return &http.Cookie{
		Name: name,
	}
}

// GetSecureCookie returns a cookie and verifies the signature value
func (r *Request) GetSecureCookie(name string, decode func(name, value string) (string, error)) (*http.Cookie, error) {
	c := r.GetCookie(name)

	if c.Value == "" {
		return nil, ErrCookieNotFound
	}

	val, err := decode(name, c.Value)
	if err != nil {
		return nil, err
	}
	c.Value = val

	return c, nil
}

// AddSecureCookie adds a secure cookie
func (r *Request) AddSecureCookie(c *http.Cookie, encode func(name, value string) (string, error)) error {
	val, err := encode(c.Name, c.Value)
	if err != nil {
		return err
	}
	c.Value = val

	r.AddCookie(c)

	return nil
}
