/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"bytes"
	"compress/flate"
	"compress/gzip"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"strings"

	"github.com/go-openapi/runtime"
	"github.com/spf13/cast"
)

type (
	// ResponseOptions defines responder options
	ResponseOptions struct {
		Encoding       string
		ContentType    string
		AcceptEncoding string
		CopyLen        int64
		Passthru       http.ResponseWriter
		WriteNull      bool
		// DEPRECATED Binary does nothing
		Binary bool
	}

	// Response defines a simple middleware.Responder struct
	Response struct {
		payload       interface{}
		status        int
		options       ResponseOptions
		body          []byte
		headerWritten bool
		headers       http.Header
		cookies       []*http.Cookie
		contentLength int
	}

	// Session is a session value map
	Session map[interface{}]interface{}

	// Compressor is a compressor interface
	Compressor interface {
		Close() error
		Flush() error
		Write(p []byte) (int, error)
	}

	encoder interface {
		Encode(w io.Writer) error
	}
)

var (
	// NoContentResponse is a no content HTTP success response
	NoContentResponse = func() *Response {
		return NewResponse().WithStatus(http.StatusNoContent)
	}

	// SeeOtherResponse returns a redirect response
	SeeOtherResponse = func(loc interface{}) *Response {
		return NewResponse().Redirect(loc).WithStatus(http.StatusSeeOther)
	}

	// FoundResponse is a found response
	FoundResponse = func(loc interface{}) *Response {
		return NewResponse().Redirect(loc).WithStatus(http.StatusFound)
	}

	// RedirectResponse is a found response
	RedirectResponse = func(loc interface{}) *Response {
		return NewResponse().Redirect(loc).WithStatus(http.StatusMovedPermanently)
	}

	// NullResponder is a black whole responder that writes headers only
	NullResponder = func() *Response {
		return NewResponse().WithOptions(ResponseOptions{WriteNull: true})
	}
)

// NewResponse returns a new response object
func NewResponse(r ...*http.Request) *Response {
	rval := &Response{
		status:  http.StatusOK,
		headers: make(map[string][]string),
		cookies: make([]*http.Cookie, 0),
	}

	return rval
}

// Copy copies a response
func (r *Response) Copy() *Response {
	body := make([]byte, len(r.body))
	copy(body, r.body)

	return &Response{
		payload: r.payload,
		status:  r.status,
		options: r.options,
		body:    body,
		headers: r.headers,
	}
}

// Length returns the response length
func (r *Response) Length() int {
	if r.body == nil {
		return 0
	}
	return len(r.body)
}

// Body returns the body
func (r *Response) Body() []byte {
	return r.body
}

// StatusCode returns the response http status code
func (r *Response) StatusCode() int {
	return r.status
}

// Payload returns the paylaod
func (r *Response) Payload() interface{} {
	return r.payload
}

// WithPayload sets the payload
func (r *Response) WithPayload(payload interface{}) *Response {
	r.payload = payload
	return r
}

// WithOptions sets the options for a response
func (r *Response) WithOptions(options ResponseOptions) *Response {

	r.options = options

	return r
}

// WithStatus sets the response http status
func (r *Response) WithStatus(s int) *Response {
	r.status = s
	return r
}

// Redirect redirects the response
func (r *Response) Redirect(loc interface{}, params ...map[string]string) *Response {
	var hloc string

	switch l := loc.(type) {
	case string:
		hloc = l
	case *string:
		hloc = *l
	case fmt.Stringer:
		hloc = l.String()
	default:
		hloc = cast.ToString(l)
	}

	if len(params) > 0 {
		u, _ := url.Parse(hloc)
		q := u.Query()

		for k, v := range params[0] {
			q.Set(k, v)
		}
		u.RawQuery = q.Encode()
		hloc = u.String()
	}

	return r.WithHeader("Location", hloc).WithStatus(http.StatusMovedPermanently)
}

// Found is a found redirect
func (r *Response) Found(loc interface{}, params ...map[string]string) *Response {
	return r.Redirect(loc, params...).WithStatus(http.StatusFound)
}

// DeleteCookies adds "delete" cookies to the response
func (r *Response) DeleteCookies(name ...string) *Response {
	for _, n := range name {
		c := &http.Cookie{
			Name:     n,
			MaxAge:   -1,
			Path:     "/",
			Value:    "",
			HttpOnly: true,
		}

		r.cookies = append(r.cookies, c)
	}

	return r
}

// SetCookie adds a cookie to the response
func (r *Response) SetCookie(c *http.Cookie) *Response {
	r.cookies = append(r.cookies, c)
	return r
}

// SetSecureCookie sets a secure cookie
func (r *Response) SetSecureCookie(c *http.Cookie, encode func(name, value string) (string, error), httpOnly ...bool) *Response {
	val, err := encode(c.Name, c.Value)
	if err != nil {
		panic(err)
	}
	c.HttpOnly = true
	c.Secure = true
	c.Value = val

	if len(httpOnly) > 0 {
		c.HttpOnly = httpOnly[0]
	}

	return r.SetCookie(c)
}

// SetCookies adds a few cookies
func (r *Response) SetCookies(c ...*http.Cookie) *Response {
	r.cookies = append(r.cookies, c...)
	return r
}

// Header implements the http.ResponseWriter
func (r *Response) Header() http.Header {
	return r.headers
}

// WithHeader sets a header
func (r *Response) WithHeader(key, value string) *Response {
	r.headers.Set(key, value)
	return r
}

// WithHeaders sets additional response headers
func (r *Response) WithHeaders(vals map[string]string) *Response {
	for k, v := range vals {
		r.headers.Set(k, v)
	}
	return r
}

// Write implements the http.ResponseWriter.Write() method
func (r *Response) Write(body []byte) (int, error) {
	if r.options.WriteNull {
		r.contentLength += len(body)
		r.headers.Set("Content-Length", cast.ToString(r.contentLength))
		return len(body), nil
	}
	if r.options.Passthru != nil {
		return r.options.Passthru.Write(body)
	}

	r.body = append(r.body, body...)

	if !r.headerWritten {
		r.WriteHeader(http.StatusOK)
	}

	return len(body), nil
}

// ContentLength return the estimated content length
func (r *Response) ContentLength() int {
	return r.contentLength
}

// WriteHeader implements the http.ResponseWriter.WriteHeader() method
func (r *Response) WriteHeader(statusCode int) {
	if r.options.Passthru != nil {
		r.options.Passthru.WriteHeader(statusCode)
		return
	}
	r.status = statusCode

	ct := r.Header().Get("Content-Type")
	if ct == "" {
		ct = http.DetectContentType([]byte(r.body))
		r.Header().Set("Content-Type", ct)
	}

	r.headerWritten = true
}

// WriteResponse implements the middleware.Responder.WriteResponse() method
func (r *Response) WriteResponse(rw http.ResponseWriter, pr runtime.Producer) {
	var body io.Reader
	var enc encoder
	var comp Compressor

	for k, s := range r.headers {
		for _, v := range s {
			rw.Header().Set(k, v)
		}
	}

	for _, c := range r.cookies {
		http.SetCookie(rw, c)
	}

	defer func() {
		if a := recover(); a != nil {
			rw.WriteHeader(http.StatusInternalServerError)
			if err, ok := a.(error); ok {
				rw.Write([]byte(err.Error()))
			}
		}
	}()

	if r.options.ContentType != "" {
		rw.Header().Set("Content-Type", r.options.ContentType)
	}

	// fixup encoding options
	if r.options.Encoding == "*" {
		r.options.Encoding = "gzip"
	}

	if len(r.body) > 0 {
		// Response was used like a writer, so send the body
		body = bytes.NewBuffer(r.body)
	} else if r.payload != nil {
		switch p := r.payload.(type) {
		case []byte:
			body = bytes.NewReader(p)
		case io.Reader:
			body = p
		case *bytes.Buffer:
			body = p
		case encoder:
			enc = p
		default:
			// User swagger to create the payload
			buf := bytes.Buffer{}
			if err := pr.Produce(&buf, r.payload); err != nil {
				rw.WriteHeader(500)
				rw.Write([]byte(err.Error()))
				return
			}
			body = &buf
		}

	}

	if (enc == nil && body == nil) || (r.headers.Get("X-Original-Method") == "HEAD") {
		// write the headers
		rw.WriteHeader(r.status)
		return
	}

	out := new(bytes.Buffer)

	// check for compression
	if strings.Contains(r.options.Encoding, "gzip") {
		rw.Header().Add("content-encoding", "gzip")
		comp = gzip.NewWriter(out)
	} else if strings.Contains(r.options.Encoding, "deflate") {
		rw.Header().Add("content-encoding", "deflate")
		comp, _ = flate.NewWriter(out, 9)
	} else {
		comp = nil
	}

	if comp != nil {
		if enc != nil {
			enc.Encode(comp)
		} else if r.options.CopyLen > 0 {
			if _, err := io.CopyN(comp, body, r.options.CopyLen); err != nil {
				panic(err)
			}
		} else {
			if _, err := io.Copy(comp, body); err != nil {
				panic(err)
			}
		}

		comp.Flush()
		comp.Close()

		rw.Header().Add("Content-Length", cast.ToString(out.Len()))
		rw.WriteHeader(r.status)
		io.Copy(rw, out)
	} else {
		if enc != nil {
			enc.Encode(out)

			rw.Header().Add("Content-Length", cast.ToString(out.Len()))
			rw.WriteHeader(r.status)

			io.Copy(rw, out)
		} else if r.options.CopyLen > 0 {
			// write the headers
			rw.Header().Set("Content-Length", cast.ToString(r.options.CopyLen))
			rw.WriteHeader(r.status)
			io.CopyN(rw, body, r.options.CopyLen)
		} else {
			// write the headers
			rw.WriteHeader(r.status)
			io.Copy(rw, body)
		}
	}
}
