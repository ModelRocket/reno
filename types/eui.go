/*************************************************************************
 * MIT License
 * Copyright (c) 2019 Model Rocket
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package types

import (
	"database/sql/driver"
	"encoding/hex"
	"errors"
	"fmt"
	"net"
)

type (
	// EUI48 is 6 byte network identifier
	EUI48 [6]byte

	// EUI64 is an 8 byte network identifier
	EUI64 [8]byte
)

// MarshalText implements encoding.TextMarshaler.
func (e EUI64) MarshalText() ([]byte, error) {
	return []byte(e.String()), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (e *EUI64) UnmarshalText(text []byte) error {
	b, err := net.ParseMAC(string(text))
	if err != nil {
		if b, err = hex.DecodeString(string(text)); err != nil {
			return err
		}
	}
	if len(e) != len(b) {
		return fmt.Errorf("sparks: exactly %d bytes are expected", len(e))
	}
	copy(e[:], b)
	return nil
}

// String implement fmt.Stringer.
func (e EUI64) String() string {
	return net.HardwareAddr(e[:]).String()
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (e EUI64) MarshalBinary() ([]byte, error) {
	out := make([]byte, len(e))
	// little endian
	for i, v := range e {
		out[len(e)-i-1] = v
	}
	return out, nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (e *EUI64) UnmarshalBinary(data []byte) error {
	if len(data) != len(e) {
		return fmt.Errorf("sparks: %d bytes of data are expected", len(e))
	}
	for i, v := range data {
		// little endian
		e[len(e)-i-1] = v
	}
	return nil
}

// Scan implements sql.Scanner.
func (e *EUI64) Scan(src interface{}) error {
	b, ok := src.([]byte)
	if !ok {
		return errors.New("sparks: []byte type expected")
	}
	b, err := net.ParseMAC(string(b))
	if err != nil {
		if b, err = hex.DecodeString(string(b)); err != nil {
			return err
		}
	}

	copy(e[:], b)

	return nil
}

// Value implements driver.Valuer.
func (e EUI64) Value() (driver.Value, error) {
	return e[:], nil
}

// MarshalText implements encoding.TextMarshaler.
func (e EUI48) MarshalText() ([]byte, error) {
	return []byte(e.String()), nil
}

// UnmarshalText implements encoding.TextUnmarshaler.
func (e *EUI48) UnmarshalText(text []byte) error {
	b, err := net.ParseMAC(string(text))
	if err != nil {
		if b, err = hex.DecodeString(string(text)); err != nil {
			return err
		}
	}
	if len(e) != len(b) {
		return fmt.Errorf("sparks: exactly %d bytes are expected", len(e))
	}
	copy(e[:], b)
	return nil
}

// String implement fmt.Stringer.
func (e EUI48) String() string {
	return net.HardwareAddr(e[:]).String()
}

// MarshalBinary implements encoding.BinaryMarshaler.
func (e EUI48) MarshalBinary() ([]byte, error) {
	out := make([]byte, len(e))
	// little endian
	for i, v := range e {
		out[len(e)-i-1] = v
	}
	return out, nil
}

// UnmarshalBinary implements encoding.BinaryUnmarshaler.
func (e *EUI48) UnmarshalBinary(data []byte) error {
	if len(data) != len(e) {
		return fmt.Errorf("sparks: %d bytes of data are expected", len(e))
	}
	for i, v := range data {
		// little endian
		e[len(e)-i-1] = v
	}
	return nil
}

// Scan implements sql.Scanner.
func (e *EUI48) Scan(src interface{}) error {
	b, ok := src.([]byte)
	if !ok {
		return errors.New("sparks: []byte type expected")
	}
	b, err := net.ParseMAC(string(b))
	if err != nil {
		if b, err = hex.DecodeString(string(b)); err != nil {
			return err
		}
	}

	copy(e[:], b)

	return nil
}

// Value implements driver.Valuer.
func (e EUI48) Value() (driver.Value, error) {
	return e[:], nil
}
